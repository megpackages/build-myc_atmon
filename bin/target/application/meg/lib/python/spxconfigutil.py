#!/usr/bin/python
import sys
import sqlite3
import subprocess
import os
import shutil
import time

loglevel = 255
islogset = False
if os.path.exists("/meg/lib/python/spxlogmngr.py"):
    sys.path.append("/meg/lib/python/")
    import spxlogmngr
    islogset = True
    
def logger(lvl, log=""):
    global loglevel
    if islogset:
        spxlogmngr.Logger(loglevel, "megixinstaller.%s.log" % time.strftime("%Y%m%d.%H%M") ).log(lvl, log, 2)
    else:        
        print "%14s  %-8s %s" % (time.strftime("%6b%2d-%2H:%2M"), lvl,  log) 

config_path             = "/meg/etc/SPXconfig.sqlite"
config_backup_path      = "/meg/etc/SPXconfig.sqlite.backup"
config_default_path     = "/meg/etc/SPXconfig.sqlite.default"
data_path               = "/media/mmc/SPXdata.sqlite"
journal_path            = "/media/mmc/journal.sqlite"
journal_default_path    = "/media/mmc/journal.sqlite.default"
config_path_old         = "/media/config/SPXconfig.sqlite"

##################### BASIC FUNCTIONS                           ####################

def connect(path, timeout=0):
    db = None
    if timeout != 0:
        db = sqlite3.connect(path, timeout)
    else:
        db = sqlite3.connect(path)

    return db

def getcursor(db):
    return db.cursor()

def commit(db):
    db.commit()
    
def close(db):
    db.close()
    
def dbpath():
    return config_path

def dbpathold():
    return config_path_old
    
def dbbackuppath():
    return config_backup_path
    
def dbdefaultpath():
    return config_default_path
    
def datapath():
    return data_path
    
def journalpath():
    return journal_path
    
def journaldefaultpath():
    return journal_default_path

################ colors class ###############

class spxcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''


##################### CONVERTCONFIG  FUNCTIONS                  ####################  

def isTableInDatabase(tablename, c):
    c.execute("pragma table_info('" + tablename + "')")
    r = c.fetchall()
    return len(r) != 0 

def isColumnInTable(tablename, columname, c):
    c.execute("pragma table_info('" + tablename + "')")
    r = c.fetchall()
    for i in range(len(r)):
        col = r[i]
        if col[1] == columname:
            return True
    return False 
    
    
##################### DATABASE FUNCTIONS                        ####################
    
def checkDbStatus(path):
    logger("ENTER")
    try:
        db = connect(path, timeout=20)
        c = getcursor(db)
        c.execute("PRAGMA integrity_check")
        logger("DEBUG", "command:<Pragma integrity check> is executing...")
        result = str(c.fetchone()[0])
    
        if result.find("ok") >=0:
            close(db)
            logger("EXIT")
            return True
        close(db)
        logger("EXIT")
        return False
    except Exception,e:
        logger("EXCEPTION", "Exception Happened: checkDbStatus %s" %str(e))
        logger("EXIT")
        return False


def compareJournal(path=None):
    logger("ENTER")
    compatible = True
    journalDflt     = connect(journaldefaultpath())
    cDflt           = getcursor(journalDflt)
    defaultJournal   = createConfigSchema(cDflt)
    close(journalDflt)

    dbJournal   = connect(journalpath())
    cSpx        = getcursor(dbJournal)
    spxJournal   = createConfigSchema(cSpx)

    for tablename in defaultJournal:
        logger("DEBUG", "Checking table:<%s>" %tablename)               

        dfltTable = defaultJournal.get(tablename)
        spxTable  = spxJournal.get(tablename)
        try:
            if spxTable == None:
                compatible = False
                close(dbJournal)
                logger("WARNING", "Table<%s> not found db" %tablename)
                logger("EXIT")
                return False
            else:
                lostCols = []
                incompCols = []
                extraCols  = []
                for colname in dfltTable:
                    if colname == "tablename":
                        continue
                    
                    logger("INFO", "Checking column:<%s>" %colname) 
                    dfltCol = dfltTable.get(colname)
                    spxCol  = spxTable.get(colname)
                    if spxCol == None:
                        compatible = False
                        close(dbJournal)
                        logger("WARNING", "Column<%s> not found in table" %colname)
                        logger("EXIT")
                        return False
                    elif not isColumnsCompatible(dfltCol, spxCol):
                        compatible = False
                        close(dbJournal)
                        logger("WARNING", "Column<%s> not compatible in table.name:<%s>" %colname %tablename)
                        logger("EXIT")
                        return False

                for colname in spxTable:
                    if colname == "tablename":
                        continue

                    dfltCol = dfltTable.get(colname)
                    spxCol  = spxTable.get(colname)
                    if dfltCol == None:
                        compatible = False
                        close(dbJournal)
                        logger("WARNING", "Column<%s> is extra in table.name:<%s>" %colname %tablename)
                        return False
                        logger("EXIT")

                for i in range(len(lostCols)):
                    colname = lostCols[i]                    
                    dfltCol = dfltTable.get(colname)
                    addColumnToTable(tablename, dfltCol, cSpx)
                    logger("INFO", "Column<%s> is added to table.name:<%s>" %colname %tablename)                

                if len(extraCols) != 0 or len(incompCols) != 0:
                    recreateTable(dfltTable, cSpx)
                    for i in range(len(extraCols)):
                        colname = extraCols[i]
                        logger("INFO", "Column<%s> is dropped from table.name:<%s>" %colname %tablename)               
                    for i in range(len(incompCols)):
                        colname = incompCols[i]
                        logger("INFO", "Column<%s> is compatibilized with table.name:<%s>" %colname %tablename)                

        except Exception as e:
            logger("EXCEPTION", "Exception:%s" %str(e))
            dbJournal.rollback()

    for tablename in spxJournal:
        dfltTable = defaultJournal.get(tablename)
        spxTable  = spxJournal.get(tablename)
        try:
            if dfltTable == None:
                logger("WARNING", "Table:<%s> not found in default config" %tablename)               
                dropTable(tablename, cSpx)
                logger("INFO", "Table:<%s> dropped from spx config" %tablename)              
        except Exception as e:
            logger("EXCEPTION", "Exception:%s" %str(e))

    
    commit(dbJournal)
    close(dbJournal)
    logger("EXIT")
    return compatible
    logger("EXIT")

    return True
    
##################### FUNCTIONS FOR CREATING META FROM DATABASE ####################

def isAutoIncrement(tablename, c):
    c.execute("SELECT 1 FROM sqlite_master WHERE tbl_name='" + tablename + "' AND sql LIKE '%AUTOINCREMENT%'")
    return (len(c.fetchall()) != 0)

def createColSchema(tablename, col, c):
#     logger("ENTER")
    COL = {}
    COL["name"] = str(col[1])
    COL["type"] = str(col[2])
    COL["notnull"] = (int(col[3]) == 1)
    COL["pk"] = (int(col[5]) == 1)
    if COL.get("pk"):
        COL["autoincr"] = isAutoIncrement(tablename, c)
#     logger("EXIT")
    return COL

def createTableSchema(tablename, c):
#     logger("ENTER")
    COLS = {}
    c.execute("pragma table_info('" + tablename + "')")
    columns = c.fetchall()
    for i in range(len(columns)):
        column = columns[i]
        colname = str(column[1])
        if colname == "tablename":
            continue
        COLS[colname] = createColSchema(tablename, column, c)
#     logger("EXIT")
    return COLS

def createConfigSchema(c):
#     logger("ENTER")
    TABLES = {}
    c.execute("SELECT name FROM sqlite_master WHERE type='table'")
    tables = c.fetchall()
    for i in range(len(tables)):
        table = tables[i]
        tablename = str(table[0])
        if tablename == "sqlite_sequence":
            continue
        TABLE = createTableSchema(tablename, c)
        TABLE["tablename"] = tablename
        TABLES[tablename] = TABLE
#     logger("EXIT")
    return TABLES

#####################  FUNCTIONS FOR PRINTING DATABASE META     ####################

def printColumn(col):
    print "type:", col.get("type")
    print "notnull:", col.get("notnull")
    print "pk:", col.get("pk")
    if col.get("pk") == 1:
        print "autoincr:", col.get("autoincr")

def printTable(table):
    print '\n'
    for column in table:
        if column == "tablename":
            continue
        print "COLUMN[", column, "]"
        printColumn(table[column])
        print '\n'

def printConfigSchema(tables):
    for table in tables:
        print "TABLE[" + table + "]"
        printTable(tables[table])
        print '\n'
    print "table count:", len(tables)

#####################    FUNCTIONS FOR OPERATION ON DATABASE    ####################

def typeDefault(type):
    if type == "TEXT":
        return ""
    else:
        return "1"

def createColumnSpec(column, defaults = {}):
    logger("ENTER")
    colname = column.get("name")
    coltype = column.get("type")
    ispk = column.get("pk")
    isautoincr = column.get("autoincr")
    isnotnull = column.get("notnull")
    dflt = defaults.get(colname)
    spec = colname + " "
    spec = spec + coltype + " "
    if ispk:
        spec = spec + " PRIMARY KEY "
        if isautoincr:
            spec = spec + "AUTOINCREMENT "
    elif isnotnull:
        spec = spec + " NOT NULL "
    if dflt != None:
        spec = spec + " DEFAULT('" + dflt + "')"
    elif isnotnull:
        spec = spec + " DEFAULT('" + typeDefault(coltype) + "')"
    logger("EXIT")
    return spec

def addColumnToTable(tablename, column, c):
    sql = "alter table " + tablename + " add column "
    sql = sql + createColumnSpec(column)
    c.execute(sql)

def dropColumnFromTable(table, colname, c):
    tmpTable = dict(table)
    tmpTable["tablename"] = tmpTable.get("tablename") + "tmp";
    del tmpTable[colname]
    createTable(tmpTable, c)
    copyFromOneTableToAnotherTable(table, tmpTable, c)
    dropTable(table.get("tablename"), c)
    renameTable(tmpTable.get("tablename"), table.get("tablename"), c)

def createTable(table, c):
    logger("ENTER")
    first = True
    sql = "CREATE TABLE " + table.get("tablename") + " (";
    for colname in table:
        if colname == "tablename":
            continue
        col = table.get(colname)
        if not first:
            sql = sql + ", ";
        else:
            first = False
        sql = sql + createColumnSpec(col)
    sql = sql + ");"
    #print "sql:", sql
    c.execute(sql)
    logger("DEBUG", "%s created" %table.get("tablename"))
    logger("EXIT")

def dropTable(tablename, c):
    logger("ENTER")
    c.execute("drop table " + tablename)
    logger("DEBUG", "%s dropped" %tablename)
    logger("EXIT")
    #print  tablename, " dropped"

def renameTable(old, new, c):
    logger("ENTER")
    c.execute("alter table " + old + " rename to " + new)
    logger("DEBUG", "%s renamed to %s" %old %new)
    logger("EXIT")

def copyFromOneTableToAnotherTable(src, dst, c):
    logger("ENTER")
    columns = ""
    first = True
    for col in dst:
        if col == "tablename":
            continue
        if not first:
            columns = columns + ", "
        else:
            first = False
        columns = columns + col

    sql = "insert into " + dst.get("tablename") + "(" + columns + ") select " + columns + " from " + src.get("tablename")
    logger("DEBUG", "copyFromOneTableToAnotherTable %s" %sql)
    c.execute(sql)
    logger("DEBUG", "copied")
    logger("EXIT")

def recreateTable(table, c):
#     logger("ENTER")
    tmpTable = dict(table)
    tmpTable["tablename"] = tmpTable.get("tablename") + "tmp";
    createTable(tmpTable, c)
    copyFromOneTableToAnotherTable(table, tmpTable, c)
    dropTable(table.get("tablename"), c)
    renameTable(tmpTable.get("tablename"), table.get("tablename"), c)
#     logger("EXIT")
    
##################### FUNCTIONS FOR CHECKING DATABASE META COMPARISON  ####################

def isColTypeCompatible(typeDflt, typeSpx):
    isComp = not ((str(typeDflt) == "TEXT" and str(typeSpx) != "TEXT") or
           (str(typeDflt) == "REAL" and str(typeSpx) == "INTEGER"))
    if not isComp:
        print spxcolors.FAIL + "\t\ttypes uncompatible dflt[", typeDflt ,"], spx[", typeSpx, "]" + spxcolors.ENDC
    return isComp

def isColNotNullCompatible(notnullDflt, notnullSpx):
    isComp = (notnullDflt == notnullSpx)
    if not isComp:
        print spxcolors.FAIL + "\t\tnotnull uncompatible dflt[", notnullDflt ,"], spx[", notnullSpx, "]" + spxcolors.ENDC
    return isComp

def isColPkCompatible(pkDflt, pkSpx):
    isComp = (pkDflt == pkSpx)
    if not isComp:
        print spxcolors.FAIL + "\t\tpk uncompatible dflt[", pkDflt ,"], spx[", pkSpx, "]" + spxcolors.ENDC
    return isComp

def isColAutoIncrCompatible(autoincrDflt, autoincrSpx):
    isComp = (autoincrDflt == autoincrSpx)
    if not isComp:
        print spxcolors.FAIL + "\t\tautoincr uncompatible dflt[", autoincrDflt ,"], spx[", autoincrSpx, "]" + spxcolors.ENDC
    return isComp

def isColumnsCompatible(colDflt, colSpx):
#     logger("ENTER")
    typeDflt = str(colDflt.get("type"))
    notnullDflt = colDflt.get("notnull")
    pkDflt = colDflt.get("pk")
    autoincrDflt = colDflt.get("autoincr")

    typeSpx = str(colSpx.get("type"))
    notnullSpx = colSpx.get("notnull")
    pkSpx = colSpx.get("pk")
    autoincrSpx = colSpx.get("autoincr")

    isComp = (isColTypeCompatible(typeDflt, typeSpx) and 
           isColNotNullCompatible(notnullDflt, notnullSpx) and
           isColPkCompatible(pkDflt, pkSpx) and 
           isColAutoIncrCompatible(autoincrDflt, autoincrSpx))

#     logger("EXIT")
    return isComp

##################### OTHER FUNCTIONS                           ####################
    
def updateAppVersion(path, version):
    logger("ENTER")
    db = connect(path)
    c = getcursor(db)
    try:
        c.execute("select min(rowid) from system_info;");
        r = c.fetchall()
        if r[0][0] == None:
            raise BaseException("error")
        c.execute("update system_info set firmware_version='" + str(version) + "' where rowid=(select min(rowid) from system_info);");
        logger("INFO", "version updated to: %s" %str(version))
    except BaseException as e:
        c.execute("insert into system_info values('Sensplorer', 'n/a', '" + str(version) + "', '*');");
        logger("INFO", "version updated to: %s" %str(version))
    commit(db)
    close(db)
    logger("EXIT")

##################### OTHER NetworkSettings                     ####################

def getNetworkSettingsFromLinux():
    logger("ENTER")
    macAddr     = "n/a"
    ipAddr      = "n/a"
    subnetMask  = "n/a"
    gatewayAddr = "n/a"
    dhcpEnabled = "n/a"
    dnsIp       = "n/a"
    p = subprocess.Popen(['/meg/bin/readIp.sh', ''], stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    out, err = p.communicate()
    networkInfo=out.split('#')
    if len(networkInfo) == 7:
        macAddr     = networkInfo[0]
        ipAddr      = networkInfo[1]
        subnetMask  = networkInfo[2]
        gatewayAddr = networkInfo[3]
        dhcpEnabled = networkInfo[4]
        dnsIp       = networkInfo[5]

    sqlinsert = "insert into network_settings(mac_addr ,ip_addr ,gateway_addr ,subnet_mask ,dhcp_enabled ,xml_server_port ,dns_server_ip)\
    values('" + macAddr + "','" + ipAddr + "','" + gatewayAddr + "','" + subnetMask + "','" + dhcpEnabled + "','1000','" + dnsIp + "');" 
    
    sqlupdate = "update network_settings set mac_addr='" + macAddr + "' , ip_addr='" + ipAddr + "' , gateway_addr='" + gatewayAddr + "' , subnet_mask='" +subnetMask + "' , dhcp_enabled='" +  dhcpEnabled + "', xml_server_port='1000',  dns_server_ip='" + dnsIp +"' where rowid = '1';"    
    logger("EXIT")
    return [sqlinsert, sqlupdate]

def updateNetworkSettings(path):
    logger("ENTER")
    networksettings = getNetworkSettings()
    logger("DEBUG", networksettings)
    db = connect(path)
    c = getcursor(db)
    try:
        c.execute("select min(rowid) from network_settings;");
        r = c.fetchall()
        if len(r) == 1 and len(r[0]) == 1:
            try:
                test = int(r[0][0])
            except BaseException as e:
                raise e
            print "update"
            c.execute(networksettings[1]);
    except BaseException as e:
        logger("EXCEPTION", "Exception: %s" %str(e))
        print "insert"
        c.execute(networksettings[0]);
    commit(db)
    close(db)
    logger("EXIT")

def getUsedVersion(path=None): #kullanilmakta olan versiyonu bulur
    logger("ENTER")
    if not path:
        path = dbpath()
    
    db = sqlite3.connect(path)
    c = db.cursor()
    r = None
    
    version = ""
    c.execute("select firmware_version from system_info")
    r = c.fetchall()
    
    for i in r:
        for j in i:
            version = j
    
    if not len(version) == 0:
        logger("ENTER")
        return version
    else:
        raise Exception("version has not been got in SPXconfig.")
    logger("EXIT")

def databasetobackup(config_path=None):
    logger("ENTER")
    if not config_path:
        config_path = dbpath()
    
    config_backup_path=dbbackuppath()
    
    if not os.path.isfile(config_path):
        raise Exception("Config is not found:%s" % config_path)
    
    shutil.copy2(config_path,config_backup_path)
    logger("DEBUG", "<%s> copied as <%s>" % (config_path, config_backup_path))
    logger("EXIT")
    
def backuptodatabase(config_path=None):
    logger("ENTER")
    if not config_path:
        config_path = dbpath()
    
    config_backup_path=dbbackuppath()
    
    if not os.path.isfile(config_backup_path):
        raise Exception("Config Backup is not found:%s" % config_backup_path)
    
    shutil.copy2(config_backup_path,config_path)
    logger("DEBUG", "<%s> copied as <%s>" % (config_backup_path, config_path))
    logger("EXIT")
    
def defaulttodatabase(config_path=None):
    logger("ENTER")
    if not config_path:
        config_path = dbpath()
    
    config_default_path=dbdefaultpath()
    
    if not os.path.isfile(config_default_path):
        raise Exception("Default Config is not found:%s" % config_default_path)
    
    shutil.copy2(config_default_path,config_path)
    logger("DEBUG", "<%s> copied as <%s>" % (config_default_path, config_path))
    logger("EXIT")
    
def defaulttojournal(journal_path=None):
    logger("ENTER")
    if not journal_path:
        journal_path = journalpath()
    
    journal_default_path = journaldefaultpath()
    
    if not os.path.isfile(journal_default_path):
        raise Exception("Default journal is not found:%s" % journal_default_path)
    
    shutil.copy2(journal_default_path,journal_path)
    logger("DEBUG", "<%s> copied as <%s>" % (journal_default_path, journal_path))
    logger("EXIT")
    
    
    

