#!/usr/bin/python

import sqlite3
import os
import imp
import sys
import time

sys.path.append("/meg/lib/python")
import spxconfigutil

loglevel = 255
islogset = False
if os.path.exists("/meg/lib/python/spxlogmngr.py"):
    sys.path.append("/meg/lib/python/")
    import spxlogmngr
    islogset = True
    
def logger(lvl, log=""):
    global loglevel
    if islogset:
        spxlogmngr.Logger(loglevel, "megixinstaller.%s.log" % time.strftime("%Y%m%d.%H%M") ).log(lvl, log, 2)
    else:        
        print "%14s  %-8s %s" % (time.strftime("%6b%2d-%2H:%2M"), lvl,  log) 

def convertToNewVersion(c):
    
    c.execute("UPDATE system_info SET firmware_version = '2.2.9'")
    logger("INFO", "firmware version updated to 2.2.9")

def convert(path=None):
    
    logger("ENTER")
    
    if not path:
        path = spxconfigutil.dbpath()
        
    db = sqlite3.connect(path)
    c = db.cursor()
    r = None
    
    convertToNewVersion(c)

    db.commit()
    db.close()
    
    logger("EXIT")
