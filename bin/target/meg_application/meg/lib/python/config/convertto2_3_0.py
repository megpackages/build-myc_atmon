#!/usr/bin/python

import sqlite3
import os
import imp
import sys
import time

sys.path.append("/meg/lib/python")
import spxconfigutil

loglevel = 255
islogset = False
if os.path.exists("/meg/lib/python/spxlogmngr.py"):
    sys.path.append("/meg/lib/python/")
    import spxlogmngr
    islogset = True
    
def logger(lvl, log=""):
    global loglevel
    if islogset:
        spxlogmngr.Logger(loglevel, "megixinstaller.%s.log" % time.strftime("%Y%m%d.%H%M") ).log(lvl, log, 2)
    else:        
        print "%14s  %-8s %s" % (time.strftime("%6b%2d-%2H:%2M"), lvl,  log) 

def addnewcolumnin_users_and_groups_groups(c, r):
    logger("ENTER")
    
    """SenPlorer 2.2.9 SPXconfig.sqlite da yer alan users_and_groups_groups tablosuna normal_calendar kolonu eklenecek.
    """
    logger("DEBUG", "convertto2_3_0 test: normal_calendar column will be included users_and_groups_groups table.")

    if not spxconfigutil.isColumnInTable("users_and_groups_groups", "normal_calendar", c):
        c.execute("select id, type, name, email_calendar, sms_calendar, dial_calendar from users_and_groups_groups")
        r = c.fetchall()
        c.execute("drop table users_and_groups_groups")
        logger("DEBUG", "table:<users_and_groups_groups> dropped")
        c.execute("CREATE TABLE users_and_groups_groups (\
                    id INTEGER PRIMARY KEY AUTOINCREMENT, \
                    type INTEGER NOT NULL,\
                    name TEXT NOT NULL,\
                    email_calendar TEXT NOT NULL,\
                    sms_calendar TEXT NOT NULL,\
                    dial_calendar TEXT NOT NULL,\
                    normal_calendar TEXT NOT NULL\
                );")
        logger("DEBUG", "created new table:<users_and_groups_groups>")
        
        for i in r:
            id                 =i[0]
            type               =i[1]
            name               =i[2]
            email_calendar     =i[3]
            sms_calendar       =i[4]
            dial_calendar      =i[5]
            normal_calendar    = "*"
            
            sql = "insert into users_and_groups_groups(id,\
                                         type,\
                                         name,\
                                         email_calendar,\
                                         sms_calendar,\
                                         dial_calendar ,\
                                         normal_calendar) values('" + str(id) + "', '" \
                                                         + str(type) + "', '" \
                                                         + str(name) + "', '" \
                                                         + str(email_calendar) + "', '" \
                                                         + str(sms_calendar) + "', '" \
                                                         + str(dial_calendar) + "', '" \
                                                         + str(normal_calendar) + "')"
                                                         
            logger("TRACE", "sql: %s" %sql)                           
            c.execute(sql)
            
        logger("INFO", "Column<%s> is inserted into table:<%s>" %("normal_callendar", "users_and_groups_groups"))
        
    logger("EXIT")

def addnewcolumnin_users_and_groups_sensorid2groups(c, r):
    logger("ENTER")
    
    """SenPlorer 2.2.9 SPXconfig.sqlite da yer alan users_and_groups_sensorid2groups tablosuna normal_enable kolonu eklenecek.
    """
    logger("DEBUG", "convertto2_3_0 test: normal_enable column will be included users_and_groups_sensorid2groups table.")

    if not spxconfigutil.isColumnInTable("users_and_groups_sensorid2groups", "normal_enable", c):
        c.execute("select id, key, groupid, sensorid, sms_enable, dial_enable, email_enable, injob_alarm, outjob_alarm from users_and_groups_sensorid2groups")
        r = c.fetchall()
        c.execute("drop table users_and_groups_sensorid2groups")
        logger("DEBUG", "table:<users_and_groups_sensorid2groups> dropped")
        c.execute("CREATE TABLE users_and_groups_sensorid2groups (\
                    id INTEGER PRIMARY KEY AUTOINCREMENT, \
                    key TEXT NOT NULL,\
                    groupid TEXT NOT NULL,\
                    sensorid INTEGER NOT NULL,\
                    sms_enable INTEGER NOT NULL,\
                    dial_enable INTEGER NOT NULL,\
                    email_enable INTEGER NOT NULL,\
                    injob_alarm INTEGER NOT NULL,\
                    outjob_alarm INTEGER NOT NULL,\
                    normal_enable INTEGER NOT NULL\
                );")
        logger("DEBUG", "created new table:<users_and_groups_sensorid2groups>")
        
        for i in r:
            id              =i[0]
            key             =i[1]
            groupid         =i[2]
            sensorid        =i[3]
            sms_enable      =i[4]
            dial_enable     =i[5]
            email_enable    =i[6]
            injob_alarm     =i[7]
            outjob_alarm    =i[8]
            normal_enable   = 1
            
            sql = "insert into users_and_groups_sensorid2groups(id,\
                                         key,\
                                         groupid,\
                                         sensorid,\
                                         sms_enable,\
                                         dial_enable,\
                                         email_enable,\
                                         injob_alarm,\
                                         outjob_alarm,\
                                         normal_enable) values('" + str(id) + "', '" \
                                                         + str(key) + "', '" \
                                                         + str(groupid) + "', '" \
                                                         + str(sensorid) + "', '" \
                                                         + str(sms_enable) + "', '" \
                                                         + str(dial_enable) + "', '" \
                                                         + str(email_enable) + "', '" \
                                                         + str(injob_alarm) + "', '" \
                                                         + str(outjob_alarm) + "', '" \
                                                         + str(normal_enable) + "')"
                                                         
            logger("TRACE", "sql: %s" %sql)                           
            c.execute(sql)
            
        logger("INFO", "Column<%s> is inserted into table:<%s>" %("normal_enable", "users_and_groups_sensorid2groups"))
        
    logger("EXIT")

def convertToNewVersion(c):
    
    c.execute("UPDATE system_info SET firmware_version = '2.3.0'")
    logger("INFO", "firmware version updated to 2.3.0")

def convert(path=None):
    
    logger("ENTER")
    
    if not path:
        path = spxconfigutil.dbpath()
        
    db = sqlite3.connect(path)
    c = db.cursor()
    r = None
    
    convertToNewVersion(c)
    addnewcolumnin_users_and_groups_groups(c, r)
    addnewcolumnin_users_and_groups_sensorid2groups(c, r)

    db.commit()
    db.close()
    
    logger("EXIT")
