#!/usr/bin/python
import sys
import imp
import os
import optparse
import time

sys.path.append("/meg/lib/python")
import spxconfigutil

loglevel = 255
islogset = False
if os.path.exists("/meg/lib/python/spxlogmngr.py"):
    sys.path.append("/meg/lib/python/")
    import spxlogmngr
    islogset = True
    
def logger(lvl, log=""):
    global loglevel
    if islogset:
        spxlogmngr.Logger(loglevel, "spxconfigcreater.%s.log" % time.strftime("%Y%m%d.%H%M") ).log(lvl, log, 2)
    else:        
        print "%14s  %-8s %s" % (time.strftime("%6b%2d-%2H:%2M"), lvl,  log)

def createConfig(path=os.getcwd(), update=False):
    logger("ENTER")
    cmd = ""
    f = None
    db = None
    try:
        f = open("config.sql", "rU")
        cmd = f.read()
        
        if os.path.exists(os.path.join(path, "SPXconfig.sqlite.default")):
            inputStr = raw_input("Are you sure??? SPXconfig.sqlite is replaced with clean SPXconfig.sqlite and (y\\n):")
            if (str(inputStr.lower()) != "y"):
                logger("INFO", "SPXconfig.sqlite didn't replaced with clean SPXconfig.sqlite")
                return
            else:
                os.remove(os.path.join(path, "SPXconfig.sqlite.default"))
                logger("DEBUG", "SPXconfig.sqlite.default is removed")
   
        db = spxconfigutil.connect(os.path.join(path, "SPXconfig.sqlite.default"))
        logger("DEBUG", "new SPXconfig.sqlite.default is created")
        c  = spxconfigutil.getcursor(db)
        
        for i in cmd.split(");"):
            try:
                if i and not i == "\n":
                    c.execute(i + ");")
                    logger("DEBUG", "< %s); > is done" %i)
            except Exception,e:
                logger("EXCEPTION", "Exception:%s" %str(e))
        if update:
            try:
                cmd = spxconfigutil.getNetworkSettingsFromLinux()[1]
                c.execute(cmd)
                logger("DEBUG", "<%s> is done" %cmd)
            except Exception,e:
                logger("EXCEPTION", "Exception:%s" %str(e))
                
        spxconfigutil.commit(db)
        
    finally:
        if db:
            spxconfigutil.close(db)
        if f:
            f.close()

def createData(path=os.getcwd()):
    cmd = ""
    f = None
    db = None
    try:
        f = open("data.sql", "rU")
        cmd = f.read()
        
        if os.path.exists(os.path.join(path, "SPXdata.sqlite.default")):
            inputStr = raw_input("Are you sure??? SPXdata.sqlite is replaced with clean SPXdata.sqlite and (y\\n):")
            if (str(inputStr.lower()) != "y"):
                logger("INFO", "SPXdata.sqlite didn't replaced with clean SPXdata.sqlite")
                return
            else:
                os.remove(os.path.join(path, "SPXdata.sqlite.default"))
                logger("DEBUG", "SPXdata.sqlite.default is removed")
        
        db = spxconfigutil.connect(os.path.join(path, "SPXdata.sqlite.default"))
        logger("DEBUG", "new SPXdata.sqlite.default is created")
        c  = spxconfigutil.getcursor(db)
        
        for i in cmd.split(");"):
            try:
                if i and not i == "\n":
                    c.execute(i + ");")
                    logger("DEBUG", "< %s); > is done" %i)
            except Exception,e:
                logger("EXCEPTION", "Exception:%s" %str(e))
                
        spxconfigutil.commit(db)
        
    finally:
        if db:
            spxconfigutil.close(db)
        if f:
            f.close()

def createJournal(path=os.getcwd()):
    filename = ""
    cmd = ""
    f = None
    db = None
    try:
        if not os.path.isdir(path):
            path,filename = os.path.split(path)

        f = open("/meg/lib/python/journal.sql", "rU")
        cmd = f.read()
        
        if os.path.exists(os.path.join(path, "journal.sqlite")):
            os.remove(os.path.join(path, "journal.sqlite.default"))
            logger("DEBUG", "journal.sqlite.default is removed")
        
        db = spxconfigutil.connect(os.path.join(path, "journal.sqlite.default"))
        logger("DEBUG", "new journal.sqlite.default is created")
        c  = spxconfigutil.getcursor(db)
        
        for i in cmd.split(");"):
            try:
                if i and not i == "\n":
                    c.execute(i + ");")
                    logger("DEBUG", "< %s); > is done" %i)
            except Exception,e:
                logger("EXCEPTION", "Exception:%s" %str(e))
                
        spxconfigutil.commit(db)
        
    finally:
        if db:
            spxconfigutil.close(db)
        if f:
            f.close()

def checkandrecoveryJournal(pathJournal=None, defaultpathJournal=None):
    if not pathJournal:
        pathJournal = spxconfigutil.journalpath()
    if not defaultpathJournal:
        defaultpathJournal = spxconfigutil.journaldefaultpath()
        
    copycommand = "cp " + defaultpathJournal + " " + pathJournal
    try:
        if os.path.exists(pathJournal):
            if os.path.exists(defaultpathJournal):
                if spxconfigutil.compareJournal():
                    logger("DEBUG", "journal.sqlite and journal.sqlite.default are equal!")
                else:
                    os.system(copycommand)
                    logger("INFO", "journal.sqlite.default is copied as journal.sqlite")
            else:
                logger("DEBUG", "Couldn't find the file:%s" %defaultpathJournal)
                createJournal(pathJournal)
                logger("DEBUG", "New database is created:%s" %defaultpathJournal)
                logger("DEBUG", "journal.sqlite.default is copying as journal.sqlite")
                os.system(copycommand)
                if not spxconfigutil.compareJournal():
                    raise Exception(("Copying is not completed!!!"))
                else:
                    logger("INFO", "journal.sqlite.default is copied as journal.sqlite succesfully")
        else:
            logger("INFO", "Couldn't find the file:%s" %pathJournal)
            if not os.path.exists(defaultpathJournal):
                createJournal(pathJournal)
                logger("DEBUG", "New database is created:%s" %defaultpathJournal)
            logger("DEBUG", "journal.sqlite.default is copying as journal.sqlite")
            os.system(copycommand)
            if not spxconfigutil.compareJournal():
                raise Exception(("Copying is not completed!!!"))
            else:
                logger("INFO", "journal.sqlite.default is copied as journal.sqlite succesfully")
    except Exception,e:
        logger("EXCEPTION", "Exception Happened: recoveryJournal %s" % e)

################## TESTER ************************
   
parser = optparse.OptionParser(usage="spxconfigcreater.py [OPTIONS]")

def option():    
    parser.add_option("-c", "--config", 
                   action="store_true",
                   help="create SPXconfig", 
                   default=False 
                   )
    parser.add_option("-u", "--update-networksettings", 
                   action="store_true",
                   help="update network settings of created SPXconfig", 
                   default=False 
                   )
    parser.add_option("-d", "--data", 
                   action="store_true",
                   help="create SPXdata", 
                   default=False 
                   )
    parser.add_option("-j", "--journal", 
                   action="store_true",
                   help="create journal", 
                   default=False 
                   )
    parser.add_option("-r", "--recovery-check-journal", 
                   action="store_true",
                   help="check and recovery journal", 
                   default=False 
                   )
    parser.add_option("-p","--path", 
                   help="path to create file",
                   default=os.getcwd() 
                   )  
    
    (options, args) = parser.parse_args() 
    
    if not (options.config or 
            options.data or 
            options.journal or
            options.recovery_check_journal ):
        parser.print_help()
        parser.error('Choose at least one option!!!')
    
    return  (options, args)

def main(argv): 
    iRslt=1      
    try:
        (options, args) = option() 
        
        if(options.config):
            createConfig(path=options.path, update=options.update_networksettings)
        if(options.data):
            createData(options.path)
        if(options.journal):
            createJournal(options.path)
        if(options.recovery_check_journal):
            checkandrecoveryJournal()

        iRslt=0      
    except BaseException, e:
        print "Exception: " + str(e)
    finally:
        print "Bye Bye"
    sys.exit(iRslt)

if __name__ == "__main__":
    main(sys.argv)
