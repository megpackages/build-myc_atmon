var Reports = {
	onLoad: null,
	close: null,
	html: null,
	devices: [],
	sensors: [],
	selects: [],
	selectnames: [],
	types: [],
	groups: [["1","5"],["3","7"],["2","6"],["4","12"]]
};

Reports.html = 	'<div class="alert"></div>'
					+'<div class="block">'
						+'<table cellpadding="0" cellspacing="0" border="0" width="100%" id="reportDevicesHead" class="tableHead">'
							+'<tr>'
								+'<td></td>'
							+'</tr>'
						+'</table>'
						+'<div id="reportDevices" class="tableDiv">'
							+'<table cellpadding="0" cellspacing="0" border="0" width="100%">'
							+'</table>'
						+'</div>'
					+'</div>'
					+'<div class="space"></div>'
					+'<div class="block">'
						+'<table cellpadding="0" cellspacing="0" border="0" width="100%" id="reportSensorsHead" class="tableHead">'
							+'<tr>'
								+'<td></td>'
							+'</tr>'
						+'</table>'
						+'<div id="reportSensors" class="tableDiv">'
							+'<table cellpadding="0" cellspacing="0" border="0" width="100%">'
							+'</table>'
						+'</div>'
					+'</div>'
					+'<div class="space"></div>'
					+'<div class="block">'
						+'<table cellpadding="0" cellspacing="0" border="0" width="100%" id="reportSelectHead" class="tableHead">'
							+'<tr>'
								+'<td><div class="clear"></div></td>'
							+'</tr>'
						+'</table>'
						+'<div id="reportSelect" class="tableDiv">'
							+'<table cellpadding="0" cellspacing="0" border="0" width="100%">'
							+'</table>'
						+'</div>'
					+'</div>';

Reports.onLoad = function() {
	Reports.devices = [];
	Reports.sensors = [];
	Reports.selects = [];
	Reports.selectnames = [];
	Reports.types = [];
	$("div#reports div.container").html(Reports.html);
	Main.loading();
	Reports.getDevices();
	
	$("div#reports #reportTypeSub").html(language.report_type_subject);
	$("div#reports #reportTimeSub").html(language.report_time_subject);
	$("div#reports #reportStartSub").html(language.report_start_subject);
	$("div#reports #reportEndSub").html(language.report_end_subject);
	$("div#reports #reportCreateButton").html(language.report_create_button);
	$("div#reports div.container div.alert").html(language.report_table_info);
	$("div#reports select#reportType option#reportCreateButton").html(language.report_graphc_view);
	$("div#reports select#reportType option#reportGraphcView").html(language.report_csv);
	$("div#reports select#reportType option#reportCsv").html(language.report_alarm_list);
	$("div#reports select#reportType option#reportMaxMin").html(language.report_max_min);
	$("div#reports select#reportTime option#reportTimePeriod").html(language.report_time_period);
	$("div#reports select#reportTime option#reportTimeLast6h").html(language.report_time_last6h);
	$("div#reports select#reportTime option#reportTimeDay").html(language.report_time_day);
	$("div#reports select#reportTime option#reportTimeLast24h").html(language.report_time_last24h);
	$("div#reports select#reportTime option#reportTimeLast7day").html(language.report_time_last7day);
	$("div#reports select#reportTime option#reportTimeThism").html(language.report_time_thism);
	$("div#reports select#reportTime option#reportTimeLast1m").html(language.report_time_last1m);
	$("div#reports table#reportDevicesHead td").html(language.report_table_devices);
	$("div#reports table#reportSensorsHead td").html(language.report_table_sensors);
	$("div#reports table#reportSelectHead td").append(language.report_table_select_sensors);
	$("div#reports table#reportSelectHead td div.clear").html(language.report_table_select_clear);

	$("div#reports").css("display","block");
};

Reports.close = function() {
	Main.loading();
	$("div#reports div.container").html("");
	$("div#reports").css("display","none");
	Reports.devices = [];
	Reports.sensors = [];
	Reports.selects = [];
	Reports.selectnames = [];
	Reports.types = [];
};

Reports.getDevices = function() {
	var getDevicesData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?getGroupDevices&groupname=" + Main.user.groupname); 
	getDevicesData.success(function() {
		Reports.devices = jQuery.parseJSON(getDevicesData.responseText);
		Reports.createDevices();
		var url = $("div#reportDevices table tr td:first").attr("data-id");
		Main.loading();
		if ($("div#reportDevices table tr").length > 0 )
			Reports.getSensors(url);
		else
			Main.unloading();
	});
	getDevicesData.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);		Main.unloading();
	});
};

Reports.sortSensors = function()
{
	var tmpList = [];
	var len = Reports.sensors.length;
	for (var i = 0; i < len; i++)
	{
		tmpList[parseInt(Reports.sensors[i].ionum)] = Reports.sensors[i];
	}

	var tmpList2 = [];
	var k = 0;
	for (var i = 0; i < tmpList.length; i++)
	{
		if (tmpList[i] != undefined)
			tmpList2[k++] = tmpList[i];
	}

	Reports.sensors = tmpList2;
};

Reports.getSensors = function(url) {
	var getSensorData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?"+ url);	 
	getSensorData.success(function() {
		Reports.sensors = jQuery.parseJSON(getSensorData.responseText);
		Reports.sortSensors();
		Reports.createSensors();
		Main.unloading();
	});	
	getSensorData.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);		Main.unloading();
	});
};

Reports.getTimeInfo = function(callback){
    
    var timeData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?getTimeInfo");

    timeData.success(function() {

        Reports.timeData = jQuery.parseJSON(timeData.responseText);
        callback(Reports.timeData);
    });
    timeData.error(function()   {
        Main.unloading();
        Main.alert(language.report_create_error);
    }); 
};


Reports.getReports = function(url, parse, cb) {
	var type = parse;
	var getReportsData = null;

	if(type == "json") {
		getReportsData = $.getJSON("http://" + hostName + "/cgi-bin/report.cgi?"+ url);
	} else {
		getReportsData = $.get("http://" + hostName + "/cgi-bin/report.cgi?"+ url);		
	}
	
	getReportsData.error(function() {
		Main.unloading();
		Main.alert(language.report_create_error);
	});
	/*.error(function(jqXHR, textStatus, errorThrown) {
        console.log("error " + textStatus);
        console.log("incoming Text " + jqXHR.responseText);
    })*/

	getReportsData.success(function() {
		Main.unloading();
		
		if(type == "json") {
			Reports.json = jQuery.parseJSON(getReportsData.responseText);
            cb(Reports.json);
			if (Reports.json.length <= 2 || getReportsData.responseText == "{\"result\":\"NODATA\"}" )
			{
				if (Reports.recordEnable == "0") // record disable ise
				{
					if (Reports.selects.length > 1)
						Main.alert(language.report_sensors_record_disabled);
					else
						Main.alert(language.report_sensor_record_disabled);	
				}
				else
		        {	
		        	Main.alert(language.report_no_data);
		        }
		        throw "sdfdg";
			}
			/*for(var i = 0; i < Reports.json.length; i++) {
				Reports.json[i].year = Reports.yearFormat(Reports.json[i].year);
			}*/
			
			if(Main.currentPage == "Reports") {
				var i = 0;
				Reports.selectnames = [];
				//for(var i = 0; i < Reports.selects.length; i++) {
					$("div#reportSelect table tr").each(
						function(){
							var obj = $(this).children("td")[0];
							var name = obj.getAttribute("data-name");
							var hwaddr =  "hwaddr=" + obj.getAttribute("data-hwaddr");
							var value = obj.getAttribute("data-id");
							var unit = obj.getAttribute("data-unit");
							var sensortype = obj.getAttribute("data-type");

							if(unit == "undefined") {
								unit = "&nbsp;";
							}

							Reports.selectnames[i] = {
								name: name,
								hwaddr: hwaddr,
								value: value,
								unit: unit,
								sensortype: sensortype,
							};
							i++;
						}
					);
				//}
			}

			Reports.createPopup();

		} else if(type == "csv") {


            Reports.getTimeInfo(function (data) {
                Reports.csv = getReportsData.responseText;
                var gmtoffserver = data.gmtoff;
                var servertime = data.servertime;

                var time = new Date((servertime * 1000) + (gmtoffserver * 1000));
                var curr_date = time.getUTCDate();
                var curr_month = time.getUTCMonth() + 1; //Months are zero based
                var curr_year = time.getUTCFullYear();
                var curr_hours = time.getUTCHours();
                var curr_minutes = time.getUTCMinutes();
                var curr_seconds = time.getUTCSeconds();
                var fullDatePath = curr_date + "-" + curr_month + "-" + curr_year + "_" + curr_hours + "-" + curr_minutes + "-" + curr_seconds;


            if (getReportsData.responseText ==  "{\"result\":\"NODATA\"}")
            {
                if (Reports.recordEnable == "0") // record disable ise
                {
                    if (Reports.selects.length > 1)
                        Main.alert(language.report_sensors_record_disabled);
                    else
                        Main.alert(language.report_sensor_record_disabled); 
                }
                else
                {   
                    Main.alert(language.report_no_data);
                }
                throw "sdfdg";
            }
            var downloadLink = document.createElement("a");

            var blob = new Blob([Reports.csv], {'type':'data:text/csv'});

            downloadLink.href = window.URL.createObjectURL(blob);
            downloadLink.download = "SensPlorerReport_" + fullDatePath + ".csv";

            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
            });

		}
         else if(type == "alarm") {


            Reports.getTimeInfo(function (data) {
                Reports.csv = getReportsData.responseText;
                var gmtoffserver = data.gmtoff;
                var servertime = data.servertime;

                var time = new Date((servertime * 1000) + (gmtoffserver * 1000));
                var curr_date = time.getUTCDate();
                var curr_month = time.getUTCMonth() + 1; //Months are zero based
                var curr_year = time.getUTCFullYear();
                var curr_hours = time.getUTCHours();
                var curr_minutes = time.getUTCMinutes();
                var curr_seconds = time.getUTCSeconds();
                var fullDatePath = curr_date + "-" + curr_month + "-" + curr_year + "_" + curr_hours + "-" + curr_minutes + "-" + curr_seconds;


			if (getReportsData.responseText ==  "{\"result\":\"NODATA\"}")
			{
				if (Reports.recordEnable == "0") // record disable ise
				{
					if (Reports.selects.length > 1)
						Main.alert(language.report_sensors_record_disabled);
					else
						Main.alert(language.report_sensor_record_disabled);	
				}
				else
		        {	
		        	Main.alert(language.report_no_data);
		        }
		        throw "sdfdg";
			}
			var downloadLink = document.createElement("a");

			var blob = new Blob([Reports.csv], {'type':'data:text/csv'});

			downloadLink.href = window.URL.createObjectURL(blob);
			downloadLink.download = "SensPlorerReports_" + fullDatePath + ".csv";

			document.body.appendChild(downloadLink);
			downloadLink.click();
			document.body.removeChild(downloadLink);

            });
		}
	});	
};


Reports.createDevices = function() {
	for(var i = 0; i < Reports.devices.length; i++) {
		var name = Reports.devices[i].name;
		var objectid = Reports.devices[i].objectid;
		var ID = Reports.devices[i].objectid.split("/");

		var getSensor = "getGroupSensors&hwaddr="+ ID[0] + "&portnum="+ ID[1] +"&devaddr="+ ID[2] + "&groupname=" + Main.user.groupname;
		var tr = document.createElement("tr");
		var tdName = document.createElement("td");
		tdName.setAttribute("data-id",getSensor);
		tdName.setAttribute("data-name",name);
		tdName.setAttribute("data-oid", objectid);
		if(i == 0) tdName.setAttribute("class","active");
		tdName.innerHTML = name;

		var span = document.createElement("span");
		span.setAttribute("class","plus");
		tdName.appendChild(span);

		tr.appendChild(tdName);
		$("div#reportDevices table").append(tr);
	}
};

Reports.createSensors = function() {
	for(var i = 0; i < Reports.sensors.length; i++) {
		var dname = $("div#reportDevices table td.active").data("name");
		var objectid = Reports.sensors[i].objectid;
		var name = Reports.sensors[i].name;
		var type = Reports.sensors[i].type;
		//var ID = Reports.sensors[i].objectid.split("/");

		//var getSensor = "getSensors&hwaddr="+ ID[0] + "&portnum="+ ID[1] +"&devaddr="+ ID[2];
		var tr = document.createElement("tr");
		var tdName = document.createElement("td");
		tdName.setAttribute("data-id",Reports.sensors[i].id);
		tdName.setAttribute("data-type",type);
		tdName.setAttribute("data-oid",  objectid);
		tdName.setAttribute("data-name",  dname + " > " + name);
		tdName.setAttribute("data-hwaddr",  Reports.sensors[i].objectid);
		tdName.setAttribute("data-unit",  Reports.sensors[i].unit);
		tdName.setAttribute("data-record",  Reports.sensors[i].record);
		tdName.setAttribute("class","type"+type);
		tdName.innerHTML = name;

		var span = document.createElement("span");
		span.setAttribute("class","plus");
		tdName.appendChild(span);

		tr.appendChild(tdName);
		$("div#reportSensors table").append(tr);
	}
};

Reports.selectSensors = function(type, content, id) {
	var status = true;
	var i = 0;

	if(Reports.selects.length > 0) {
		for(i = 0; i < Reports.selects.length; i++) {

			if(Reports.selects[i] == id) {
				status = false;
			}
		}
	} else {
		Reports.selects[0] = id;
	}

	if(status && Reports.sensorGroup(type)) {
		Reports.selects[i] = id;
		var tr = document.createElement("tr");
		tr.setAttribute("id","selects-" + i);
		tr.innerHTML = content;
		$("div#reportSelect table").append(tr);
	}

	if(!status) {
		Main.alert(language.report_againsensorselect);
	} else if(!Reports.sensorGroup(type)){
		Main.alert(language.report_notsensorselect);
	}
};

Reports.sensorGroup = function (type) {
	var count = true;
	var status = true;
	var i = 0;

	for(i = 0; i < Reports.groups.length; i++) {
		for(var k = 0; k < Reports.groups[i].length; k++) {
			if(type == Reports.groups[i][k]) {	
				if(Reports.types != [] && Reports.types != "" && Reports.types != null) {
					if(i != Reports.types[0]) {
						count = false;
					}
				} else {
					Reports.types[0] = i;
				}
			}
		}
	}

	if(count) {
		status = true;
	} else {
		status = false;
	}

	return status;
};

Reports.createReportScreen = function(type, mtime, record) {

	var dateSelect = false;
	var reportsTime = null;
	reportsTime = mtime;
	if(mtime == undefined) {
		reportsTime = $("#reportTime").val();
	}

	var reportStartTime = $("div#reports #from").val();
	var reportEndTime = $("div#reports #to").val();

	if(parseInt(reportsTime) == 0)
	{
		if(reportStartTime.length == 0 && reportEndTime.length == 0)
		{
			Main.alert(language.alert_empty_start_end_date);
			return 0;
		}
		else if(reportStartTime.length == 0)
		{
			Main.alert(language.alert_empty_start_date);
			return 0;
		}
		else if(reportEndTime.length == 0)
		{
			Main.alert(language.alert_empty_end_date);
			return 0;
		}	
	}

	if(type == "alarm") {
		getData = "getStatusChangesbyInterval";
		type = "csv";
	}
	else if (type == "maxmin")
	{	
		type = "csv";
		getData = "getMaxMinbyInterval";	
	}
	 else {
		getData = "getDatabyInterval";		
	}

    Main.loading();
    Reports.callReports(type, reportsTime, reportStartTime, reportEndTime, getData);

};

Reports.getReportItem = function() {
	var item = "[";
	if ($("div#reportSensors").length != 0)
		Reports.recordEnable = $("div#reportSelect td[data-record='1']").length > 0 ? "1":"0" ;
	if(Reports.selects.length > 1) {
		for(var i = 0; i < Reports.selects.length; i++) {
			if(i != Reports.selects.length-1) {
				item+= Reports.selects[i] + ",";						
			} else {
				item+= Reports.selects[i] + "]";
			}
		}
	} else {
		item = Reports.selects[0];
	}

	return item;
};

/*Reports.yearFormat = function(unix) {
	var time = new Date(unix*1000);
	return time;
};*/

Reports.callReports = function(type, intervalType, from, to, getData) {

	var item = Reports.getReportItem();
	var url = getData + "&type=" + type + "&intervalType=" + intervalType + "&from=" + from + "&to=" + to + "&sensors=" + item;
	try
	{
	   Reports.getReports(url, type, function (data) {

            var stime = parseInt(data[0].year);
            var etime = parseInt(data[data.length-1].year);
            var gmtOffServer = parseInt(data[0].gmtoff);


            var startTime = new Date((stime*1000) + (gmtOffServer*1000));
            var endTime = new Date((etime*1000) + (gmtOffServer*1000));
            var curr_date = endTime.getUTCDate();
            if(curr_date < 10) curr_date = "0" + curr_date;
            var curr_month = endTime.getUTCMonth() + 1;
            if(curr_month < 10) curr_month = "0" + curr_month;
            var curr_year = endTime.getUTCFullYear();
            var curr_hours = endTime.getUTCHours();
            if(curr_hours < 10) curr_hours = "0" + curr_hours;
            var curr_minutes = endTime.getUTCMinutes();
            if(curr_minutes < 10) curr_minutes = "0" + curr_minutes;
            var curr_seconds = endTime.getUTCSeconds();
            if(curr_seconds < 10) curr_seconds = "0" + curr_seconds;
            var endfullDatePath = curr_date + "/" + curr_month + "/" + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds;

            var start_date = startTime.getUTCDate();
            if(start_date < 10) start_date = "0" + start_date;
            var start_month = startTime.getUTCMonth() + 1;
            if(start_month < 10) start_month = "0" + start_month;
            var start_year = startTime.getUTCFullYear();
            var start_hours = startTime.getUTCHours();
            if(start_hours < 10) start_hours = "0" + start_hours;
            var start_minutes = startTime.getUTCMinutes();
            if(start_minutes < 10) start_minutes = "0" + start_minutes;
            var start_seconds = startTime.getUTCSeconds();
            if(start_seconds < 10) start_seconds = "0" + start_seconds;
            var startfullDatePath = start_date + "/" + start_month + "/" + start_year + " " + start_hours + ":" + start_minutes + ":" + start_seconds;
            
            $("#content div#reportPopup div#reportSubject").html(language.reportdate + " : " +startfullDatePath + " & " + endfullDatePath);

        });
	}
	catch(err)
	{
		return;
	}
};

Reports.createPopup = function() {
	$("div#reportPopup").css("display", "block");
	$("div#overlay").css("display", "block");


	generateChartGraph();
};

$(function() {
    
    $( "div#reports #from" ).datepicker({
      defaultDate: "+1w",
      changeMonth: false,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "div#reports #to" ).datepicker( "option", "minDate", selectedDate );
      }
    });

    $( "div#reports #to" ).datepicker({
      defaultDate: "+1w",
      changeMonth: false,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "div#reports #from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

	$("div#reports").on( "click", "div#reportDevices table td", function() {
		$("div#reportSensors table").html("");
		var url = $(this).attr("data-id");
		$("div#reportDevices table td").removeClass("active");
		$(this).addClass("active");
		Main.loading();
		Reports.getSensors(url);
	});	

	$("div#reports").on("change", "select#reportTime", function() {
		if ($(this).prop("selectedIndex") != "0")
		{
			$( "div#reports #from" ).prop("value", "");
			$( "div#reports #to" ).prop("value", "");
			$( "div#reports #from" ).prop("disabled", true);
			$( "div#reports #to" ).prop("disabled", true);
		}
		else
		{
			$( "div#reports #from" ).prop("disabled", false);
			$( "div#reports #to" ).prop("disabled", false);
		}
	});

	$("div#reports").on( "click", "div#reportSensors table td", function() {
		if(Reports.selects.length != 6) {
			
		$("#tooltip").remove();
			var type = $(this).data("type");
			var id = $(this).data("id"); 
			var content = $(this).parent().html();
			Reports.selectSensors(type, content, id);
		} else {
			Main.alert(language.report_maxsensor_size);
		}
	});	

	$("div#reports").on( "click", "div#reportSelect table tr", function() {
		var content = $(this).attr("id");
		var element = content.split("-");
		var index = $(this).index();
		var id = $("#" + content + " td").attr("data-id");
		Reports.selects.splice(index, 1);
		$(this).remove();
		if(Reports.selects.length == 0) {
			Reports.types = [];			
		}
	});	

	$("div#reports").on( "click", "table#reportSelectHead div.clear", function() {
		Reports.selects = [];
		Reports.types = [];
		$("div#reportSelect table").html("");
	});

	$("div#content").on( "click", "div#reportPopup div#closePopup", function() {
		$("div#reportPopup").css("display", "none");
		$("div#overlay").css("display", "none");
	});

	$("div#reports").on( "click", "div.createFilter div#reportCreateButton", function() {
		if(Reports.selects.length > 0) {

			var reportsType = $("#reportType").val();

			if(reportsType == "json") {
				Reports.createReportScreen(reportsType);
			} else if(reportsType == "csv") {
				Reports.createReportScreen(reportsType);
			} else if(reportsType == "alarm") {
				Reports.createReportScreen(reportsType);
			} else if(reportsType == "maxmin") {
				Reports.createReportScreen(reportsType);		
		} else {
			Main.alert(language.report_no_sensor_select);
		}
		}
	});
	$("div#reports").on("mouseover", "div#reportDevices td", function() {
		var title = $(this).attr("data-oid");
		if(title != undefined ) {
			var t = document.createElement("b");
			t.setAttribute("id","tooltip");
			t.innerHTML = title;
			$(this).append(t);
		}
	});

	$("div#reports").on("mouseout", "div#reportDevices td", function() {
		$("#tooltip").remove();
	});


	$("div#reports").on("mouseover", "div#reportSensors td", function() {
		var title = $(this).attr("data-oid");
		if(title != undefined ) {
			var t = document.createElement("b");
			t.setAttribute("id","tooltip");
			t.innerHTML = title;
			$(this).append(t);
		}
	});

	$("div#reports").on("mouseout", "div#reportSensors td", function() {
		$("#tooltip").remove();
	});
});
