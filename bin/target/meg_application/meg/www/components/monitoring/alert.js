var totalJSON = new Array();
var alarmJSON = new Array();
var warningJSON = new Array();
var lostJSON = new Array();
var normalJSON = new Array();
var inactiveJSON = new Array();
var outputJSON = new Array();
var notificationActive = false;

var html = '<div id="total" class="balloon">'
				+ '<span class="name"></span>'
				+ '<span class="count"></span>'
				+ '</div>'
				+ '<div id="alarm" class="balloon">'
				+ '	<span class="name"></span>'
				+ '	<span class="count"></span>'
				+ '</div>'
				+ '<div id="warning" class="balloon">'
				+ '	<span class="name"></span>'
				+ '	<span class="count"></span>'
				+ '</div>'
				+ '<div id="lost" class="balloon">'
				+ '	<span class="name"></span>'
				+ '	<span class="count"></span>'
			+ '</div>';

Monitoring.createAlertPanel = function() {

	$("div#content div#header div#alertPanel").html(html);

	jsonList = Main.filterControl();

	var totalTotal = 0;
	var alarmTotal = 0;
	var warningTotal = 0;
	var normalTotal = 0;
	var lostTotal = 0;
	var outputTotal = 0;
	var inactiveTotal = 0;
	var total = jsonList.length;
	alarmJSON = new Array();
	warningJSON = new Array();
	lostJSON = new Array();
	outputJSON = new Array();
	normalJSON = new Array();
	inactiveJSON = new Array();
	totalJSON = jsonList;

	for(var i = 0; i < jsonList.length; i++) {

		var status = Monitoring.statusControl(jsonList[i].status);

		if(status==language.warning) {
			warningJSON[warningTotal] = jsonList[i];
			warningTotal++;
		} else if(status==language.alarm) {
			alarmJSON[alarmTotal] = jsonList[i];
			alarmTotal++;
		} else if(status==language.lost) {
			lostJSON[lostTotal] = jsonList[i];
			lostTotal++;
		} else if(status==language.normal) {
			normalJSON[normalTotal] = jsonList[i];
			normalTotal++;
		} else if(status==language.inactive) {
			inactiveJSON[inactiveTotal] = jsonList[i];
			inactiveTotal++;
		} else if(status==language.output) {
			outputJSON[outputTotal] = jsonList[i];
			outputTotal++;
		}
	}

	$("div#alertPanel div#total span.name").html(language.header_total);
	$("div#alertPanel div#total span.count").html(total);
	if (total == 0) $("div#alertPanel div#total").addClass("zerocount");
	else $("div#alertPanel div#total").removeClass("zerocount");

	$("div#alertPanel div#alarm span.name").html(language.header_alarm);
	$("div#alertPanel div#alarm span.count").html(alarmTotal);
	if (alarmTotal == 0) $("div#alertPanel div#alarm").addClass("zerocount");
	else $("div#alertPanel div#alarm").removeClass("zerocount");

	$("div#alertPanel div#warning span.name").html(language.header_warning);
	$("div#alertPanel div#warning span.count").html(warningTotal);
	if (warningTotal == 0) $("div#alertPanel div#warning").addClass("zerocount");
	else $("div#alertPanel div#warning").removeClass("zerocount");

	$("div#alertPanel div#lost span.name").html(language.header_lost);
	$("div#alertPanel div#lost span.count").html(lostTotal);
	if (lostTotal == 0) $("div#alertPanel div#lost").addClass("zerocount");
	else $("div#alertPanel div#lost").removeClass("zerocount");
};

$(function() {
	$("div#content").on( "click", "div#header div.balloon:not(.zerocount)", function() {
		var id = $(this).attr("id");
		if (id == "notifications")
		{
			notificationActive = true;
			if(!Main.pages[0].status)
			{
				Main.pagesClose();
				Main.pages[0].status = true;
				Monitoring.onLoad("true");
				Main.currentPage = Main.pages[0].type;
				$("div#menu li.active").removeClass("active");
				$("div#menu li#Monitoring").addClass("active");
				
				refreshType = "total";
				$("#sensorNameDel").css("display","none");
			  	$('#sensorName').removeClass("focus");
			  	$('#sensorName').val(language.sensorname);

			  	$("#deviceNameDel").css("display","none");
			  	$('#deviceName').removeClass("focus");
			  	$('#deviceName').val(language.devicename);
				jsonList = eval("totalJSON");
				Monitoring.searchArray = jsonList;

				console.log(Main.fullItems);
				//Monitoring.clearElement();
				Main.searchRequest(Monitoring.searchArray,1);
				Main.createSuspendedDeviceList();
				Main.unloading();
			}
			$("div#content div#header div.balloon").removeClass("active");
			$(this).addClass("active");
			$("div#monitoring div.tableContainer").css("display", "none");
			$("div#monitoring div#pager").css("display", "none");
			$("div#monitoring div.NotificationArea").css("display","block");
			$("div#monitoring div.NotificationArea table#header tr a").html(language.notification_pano);
			$("#content div#main div.NotificationArea div#noNotification").css("display", "none");
			$("div.NotificationArea table#suspendedDevices tr:not(#header)").css("display", "none");
			$("div.NotificationArea table#suspendedDevices tr#header").css("display", "table-row");
			$("div.NotificationArea table#suspendedDevices tr#header span").attr("class", "downArrow");
		}
		else
		{
			notificationActive = false;
			var items = eval(id + "JSON");
			if(items != "")
			{
				$("div#content div#header div.balloon").removeClass("active");
				$(this).addClass("active");
				$("div#monitoring div.tableContainer").css("display","block");
				$("div#monitoring div.NotificationArea").css("display","none");
				$("div#monitoring div#pager").css("display", "block");
	
				if(!Main.pages[0].status)
				{
					Main.pagesClose();
					Main.pages[0].status = true;
					Monitoring.onLoad("true");
					Main.currentPage = Main.pages[0].type;
					$("div#menu li.active").removeClass("active");
					$("div#menu li#Monitoring").addClass("active");
					
					refreshType = id;
					$("#sensorNameDel").css("display","none");
				  	$('#sensorName').removeClass("focus");
				  	$('#sensorName').val(language.sensorname);
	
				  	$("#deviceNameDel").css("display","none");
				  	$('#deviceName').removeClass("focus");
				  	$('#deviceName').val(language.devicename);
					jsonList = eval(id + "JSON");
					Monitoring.searchArray = items;
	
					console.log(Main.fullItems);
					//Monitoring.clearElement();
					Main.searchRequest(Monitoring.searchArray,1);
					Main.createSuspendedDeviceList();
					Main.unloading();
	
				}
				else
				{
					refreshType = id;
					$("#sensorNameDel").css("display","none");
				  	$('#sensorName').removeClass("focus");
				  	$('#sensorName').val(language.sensorname);
	
				  	$("#deviceNameDel").css("display","none");
				  	$('#deviceName').removeClass("focus");
				  	$('#deviceName').val(language.devicename);
					jsonList = eval(id + "JSON");
					Monitoring.searchArray = eval(id + "JSON");
					//Monitoring.clearElement();
					Main.searchRequest(Monitoring.searchArray,1);
				}
			}
			else
			{
				Main.alert(language.notstatusalert);
				Main.unloading();
			}
		}

	});
});