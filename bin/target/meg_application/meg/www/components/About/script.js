var About = {
    onLoad: null,
    close: []

};

About.html = '<a href="http://www.sensplorer.com" target="_blank"><img src="../../images/status/sensplorer.jpg" style="width:330px;height: 150px;margin-left: 210px;top: 20px"></a><br>'
    +'<p class="text"><span style="color:darkorange">KRİTİK MEKANLARI HER SANİYE TAKİP EDİN</span>   Çevre değerlerini ve cihaz parametrelerini sürekli  takip eder, uyarı ve alarm durumu oluştuğunda, oluşan sorunun tanımlandığı kişilere bilgi verir</p><br>'
    +'<p class="text"><span style="color:darkorange">FARKLI VERİ/PARAMETRE İZLEME</span>	 Sensplorer ürün ailesinden modül ve sensörler dışında, SNMP,  IEC 60870-104, Modbus RTU, Modbus TCP, SEC, Megatech, IEC1107…</p><br>'
    +'<p class="text"><span style="color:darkorange">ÇEŞİTLİ ALARM BİLDİRİMLERİ</span>	   E-Mail, SMS, WEB-SMS, SSL üzerinden MS Azure Web ServisSNMP trap, Modbus TCP, XML,  Syslog…</p><br>'
    +'<p class="text"><span style="color:darkorange">WEB ÜZERİNDEN İZLEME / RAPORLAMA</span>	 Sadece kullandığınız web gezgini ile Sensplorer verileri takip edilebilir.Tanımlanmış veriler , istenen aralıkla saklanır. Rapor alınabilir, rapor çıktısı CSV olarak indirilebilir</p><br>'
    +'<p class="text"><span style="color:darkorange">TEK BAŞINA YETERLİ</span>   Sensplorer, sürekli takip ettiği değerlerin durum değişimini izleyip alarm üretirken başka hiçbir donanım ve yazılıma gereksinim duymaz.Diğer donanım, işletim sistemi ve yazılımların sorunlarından etkilenmez.</p><br>'
    +'<p class="text"><span style="color:darkorange">MODÜLER GENİŞLETİLEBİLİR YAPI</span>	 İhtiyaç olduğu zaman ihtiyaç miktarında sensör, giriş veya çıkış modülleri eklenerek sistem genişletilebilir.</p><br>'
    +'<p class="text" style="font-weight: bold;color: black;"><a target="_blank" href="ftp://SPX:sensplorer910@ftp.meg.com.tr/Documents/SPX-B1016.pdf" style="text-decoration: none;color: black">SensplorerX Donanımı Kullanım Kılavuzu için tıklayın…</a></p><br>'
    +'<span class="text" style="font-weight: bold;color: black"><a target="_blank" href="ftp://SPX:sensplorer910@ftp.meg.com.tr/Documents/SensplorerX_SuperUser_Guide_v214.pdf" style="text-decoration: none;color: black">SensplorerX Web Arayüzü Kullanım Kılavuzuiçin tıklayın…</a></span><br><br>'
    +'<a href="http://www.meg.com.tr" target="_blank"><img src="../../images/status/meg.jpg" style="width:395px;height: 150px"></a>'
    +'<span class="text" style="position: absolute;font-size: larger;color: blue;font-weight: bolder">Meg Elektrik-Elektronik Bilgi ve İletişim Sis.Ltd.Şti.</span><br>'
    +'<span class="text" style="position: absolute;right: 7px;bottom: 192px;font-weight: bolder;font-family: sans-serif">İTÜ Ayazağa Kampüsü , ARI 1 Teknokent No:23</span><br>'
    +'<span class="text" style="position: absolute;right: 7px;bottom: 176px;font-weight: bolder;font-family: sans-serif">Maslak SARIYER-İSTANBUL TURKEY</span><br>'
    +'<span class="text" style="position: absolute;right: 7px;bottom: 161px;font-weight: bolder;font-family: sans-serif">Telefon:+90(212) 287 34 07</span><br>'
    +'<span class="text" style="position: absolute;right: 7px;bottom: 146px;font-weight: bolder;font-family: sans-serif">Fax:+90(212) 287 33 48</span><br>'
    +'<span class="text" style="position: absolute;right: 7px;bottom: 132px;font-weight: bolder;font-family: sans-serif;font-size: larger"><a target="_blank" href="http://www.meg.com.tr" style="text-decoration: none;color: darkred">www.meg.com.tr</a></span><br>';

About.onLoad = function(){
    Main.loading();
    $("div#about").html(About.html);
    $("div#about").css("display","block");
    Main.unloading();
};

About.close = function ()
{
    Main.loading();
    $("div#about").html("");
    $("div#about").css("display","none");

};

$(function(){
    $("div#about").on( "click", "ul.tabs li", function() {
        alert("asdsa");
        Main.loading();
        var func = $(this).data("func");
        $("div#about ul.tabs li.active").removeClass("active");
        $(this).addClass("active");
        eval("About." + func);

    });
});