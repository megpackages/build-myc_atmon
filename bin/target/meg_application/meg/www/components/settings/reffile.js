var RefFilesCreate = {};

Settings.callRefFileGenerate = function()
{
	$("div#settings div.form").css("padding","0");
	$("div#settings div.form").css("width","100%");
	$("div#settings div.form").html("");
	var html =    '<div id="generate">'
					+ '<ul class="filters">'
						+ '<li class="checkboxs">'
							+ '<div class="name" id="typename"></div>'
							+ '<label for="binary" id="binaryName" style="display: none;"></label>'
							+ '<label for="float" id="floatName" style="display: none;"></label>'
							+ '<label for="output" id="outputName" style="display: none;"></label>'
							+ '<label for="string" id="stringName" style="display: none;"></label>'
							+ '<label for="bitstring" id="bitstringName" style="display: none;"></label>'
						+ '</li>'
					+ '</ul>'
					+ '<div class="alert"></div>'
					+ '<div id="Header">'
						+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="list">'
						+ '</table>'				
					+ '</div>'
					+ '<div id="editSensors">'
						+ '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="list">'
						+ '</table>'				
					+ '</div>'
					+ '<div class="information">'
						+ '<div id="buttons" class="buttons">'
							+ '<input type="text" id="filename" value placeholder="' + language.reffilename + '">'
							+ '<input  type="submit" id="saveall" style="display: inline-block; width: 150px;">'
							+ '	<input type="submit" id="Cancel" style="display: inline-block; width: 150px;">'
							+ '	<input type="submit" id="newsensor" style="display: inline-block; width: 150px;">'
						+ '</div>'
						+ '<div class="infos">'
							+ '<div class="blk" id="infoEdit"><input id="edit" type="submit"><b></b></div>'
							+ '<div class="blk" id="infoSave"><input id="save" type="submit"><b></b></div>'
							+ '<div class="blk" id="infoDel"><input id="delete" type="submit"><b></b></div>'
							+ '<div class="blk" id="infoCancel"><input id="cancel" type="submit"><b></b></div>'
						+ '</div>'
					+ '</div>'
				+ '</div>'
				+ '<div id="alertRemove" style="width: 400px;" class="panelDiv">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
					+ '	<input type="submit" id="buttonClose" name="buttonClose" class="submit">'
					+ '</div>'
				+ '</div>'	
				+ '<ul class="element" id="selectform">'
					+'<li id="type">'
						+ '<div class="left"></div>'
						+ '<div class="right">'
							+ '<select>'
							+ '</select>'
						+ '</div>'
					+ '</li>'
					+	'<li id="reffiles">'
						+ '<div class="left"></div>'
						+ '<div class="right">'
							+ '<select>'
							+ '</select>'
						+ '</div>'
					+ '</li>'
					+ '<li id="buttons">'
					    + ' <input type="file" style="display:none" id="buttonAddRefFile" name="buttonAddRefFile" class="submit">'
						+ '	<input type="submit" id="buttonAdd" name="buttonAdd" class="submit" style="font-size:12px;">'
						+ '	<input type="submit" id="buttonAddRef" name="buttonAddRef" class="submit" style="font-size:12px;">'
						+ '	<input type="submit" id="buttonUpdate" name="buttonUpdate" class="submit" style="font-size:12px;">'
					+ '</li>'
				+ '</ul>';
	$("div#settings div.form").html(html);

	$("div#settings ul#selectform li#type div.left").html(language.devices_type);
	$("div#settings ul#selectform li#reffiles div.left").html(language.devices_ref_file);
	$("div#settings ul#selectform li#buttons input#buttonAdd").val(language.reffile_create_add);
	$("div#settings ul#selectform li#buttons input#buttonAddRef").val(language.reffile_create_addref);
	$("div#settings ul#selectform li#buttons input#buttonUpdate").val(language.reffile_create_update);
	$("div#settings div#generate li.checkboxs div#typename").append(language.Settings_edits_typename);

	$("div#settings div#generate div.information div#infoDel b").html(language.Settings_edits_info_delete);
	$("div#settings div#generate div.information div#infoCancel b").html(language.Settings_edits_info_cancel);
	$("div#settings div#generate div.information div#infoSave b").html(language.Settings_edits_info_save);
	$("div#settings div#generate div.information div#infoEdit b").html(language.Settings_edits_info_edit);

	$("div#settings div#generate div.information div#infoDel input").val(language.Settings_edits_delete);
	$("div#settings div#generate div.information div#infoCancel input").val(language.Settings_edits_cancel);
	$("div#settings div#generate div.information div#infoSave input").val(language.Settings_edits_save);
	$("div#settings div#generate div.information div#infoEdit input").val(language.Settings_edits_edit);

	$("div#settings div#generate div.information input#saveall").val(language.save);
	$("div#settings div#generate div.information input#newsensor").val(language.Settings_edits_newsensor);
	$("div#settings div#generate div.information input#Cancel").val(language.cancel);

	$("div#settings div#generate  label#binaryName").html(language.Settings_edits_binary);
	$("div#settings div#generate  label#floatName").html(language.Settings_edits_float);
	$("div#settings div#generate  label#outputName").html(language.Settings_edits_output);
	$("div#settings div#generate  label#stringName").html(language.Settings_edits_string);
	$("div#settings div#generate  label#bitstringName").html(language.Settings_edits_bitstring);
	$("div#settings div#generate").css("display", "none");

	for (var key in deviceTypes)
	{
		if (deviceTypes[key].reffileneeded)
		{
			var opt = document.createElement("option");
			opt.setAttribute("id", key);
			opt.setAttribute("value", (deviceTypes[key].value).toString());
			opt.innerHTML = eval("language." + key);
			$("div#settings ul#selectform li#type select").append(opt);
		}
	}

	$("div#settings ul#selectform li#type select").prop("selectedIndex", 0);

	RefFilesCreate.binaryCount = 0; 
	RefFilesCreate.floatCount = 0; 
	RefFilesCreate.outputCount = 0;
	RefFilesCreate.stringCount = 0;
	RefFilesCreate.bitstringCount = 0;
	RefFilesCreate.totalCount  = 0;
	RefFilesCreate.getRefFiles();
};

RefFilesCreate.getRefFiles = function()
{
	Main.loading();
	var url = "get" + Reffiles[$("div#settings ul#selectform li#type select option:selected").attr("id")] +"RefFiles";
	var reffilesData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?" + url);	 
	reffilesData.success(function() {
		var items = JSON.parse(reffilesData.responseText);
			$("div#settings ul#selectform li#reffiles select").html("");
		for (var i =0 ; i < items.length; i++)
		{
			var opt = document.createElement("option");
			opt.setAttribute("value", items[i].name);
			opt.setAttribute("loc", items[i].loc);
			opt.innerHTML = items[i].name;
			$("ul#selectform li#reffiles select").append(opt);
		}
		Main.unloading();
	});
	reffilesData.error(function(jqXHR, textStatus, errorThrown){	
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	});
};

RefFilesCreate.getRefFile = function()
{
	Main.loading();
	var deviceType = $("div#settings ul#selectform li#type select option:selected").attr("value");
	var url = "getRefFile&name=" + $("div#settings ul#selectform li#reffiles select option:selected").attr("value") +"&device_type=" + deviceType;
	var reffilesData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?" + url);	 
	reffilesData.success(function() {
		var items = JSON.parse(reffilesData.responseText);
		RefFilesCreate.editsSensorList = items;
		RefFilesCreate.filename = $("div#settings ul#selectform li#reffiles select option:selected").attr("value");
		RefFilesCreate.sortSensors();
		RefFilesCreate.createEditSensorsList();
		Main.unloading();
	});
	reffilesData.error(function(jqXHR, textStatus, errorThrown){	
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
	    	Main.unloading();
	});
};

RefFilesCreate.sortSensors = function()
{
	maxIonum = "0";
	var tmpList = [];
	var len = RefFilesCreate.editsSensorList.length;
	for (var i = 0; i < len; i++)
	{
		if (parseInt(RefFilesCreate.editsSensorList[i].ionum) > parseInt(maxIonum))
			maxIonum = RefFilesCreate.editsSensorList[i].ionum;
		tmpList[parseInt(RefFilesCreate.editsSensorList[i].ionum)] = RefFilesCreate.editsSensorList[i];
	}

	var tmpList2 = [];
	var k = 0;
	for (var i = 0; i < tmpList.length; i++)
	{
		if (tmpList[i] != undefined)
			tmpList2[k++] = tmpList[i];	
	}

	RefFilesCreate.editsSensorList = tmpList2;
};

RefFilesCreate.addSensor = function(item)
{
	if (item.ionum == undefined)
	{		
		item.ionum = (RefFilesCreate.ionum + 1).toString();
		RefFilesCreate.ionum = RefFilesCreate.ionum + 1;
	}
	
	if (typeof(firstSelect) == "undefined")
		firstSelect = "";
	if (typeof(displayStatus) == "undefined")
	{	
		displayStatus = "input";
	}
	var sensType = sensorTypesToStr2(parseInt(item.type));
	var tr = document.createElement("tr");
	tr.setAttribute("class", sensType);
	tr.setAttribute("id","sensor"+item.ionum);
	var params = sensorParams[sensType];
	for (var j = 0; j < params.length; j++)
	{
		var param = params[j];
		if (param.name == "resolution" && item.resolution == undefined)
			item.resolution = "1";
		var opt = param.dispref;
		if (param.name == "read_period")
			continue;
		if (param.name == "read_enable")
			continue;
		if (opt == undefined)
			continue;
		if ($(tr).has("td#" + param.name).length != 0)
			continue;
		var td1 = document.createElement("td");
		td1.setAttribute("id", param.name);
		td1.setAttribute("access", (param.access).toString());
		td1.setAttribute("class",opt );

		var value = eval("item." + param.name);

		if (param.html == "text")
		{
			if (param.access == access.rw || param.access == access.w)
			{
				var input = document.createElement("input");
				input.setAttribute("type","text");	
				input.setAttribute("data-type", param.type);
				input.setAttribute("data-name", param.name);
				if (param.min != undefined)
					input.setAttribute("minval", param.min);
				if (param.max != undefined)
					input.setAttribute("maxval", param.max);
				if (param["default"] != undefined)
					input.setAttribute("default", param["default"]);
				if (value != undefined)
					input.setAttribute("value", value);
				else
					input.setAttribute("value", param["default"]);
				input.setAttribute("disabled", "disabled");
				td1.appendChild(input);
			}
		}
		else if (param.html == "checkbox")
		{
			if (param.access == access.rw || param.access == access.w)
			{
				var div = document.createElement("div");
				var input = document.createElement("input");
				input.setAttribute("type","checkbox");	
				input.setAttribute("data-type", param.type);
				input.setAttribute("data-name", param.name);
				input.checked = (value == "1" ? true:false);
				input.setAttribute("disabled", "disabled");
				input.setAttribute("class", "squaredFour");
				div.appendChild(input);
				td1.appendChild(div);
			}
		}
		else if (param.html == "select")
		{
			if (param.access == access.rw || param.access == access.w)
			{
				var select = document.createElement("select");	
				select.setAttribute("func", param.func);
				select.setAttribute("data-name", param.name);
				select.selectedIndex = parseInt(value);
				select.setAttribute("disabled", "disabled");

				if (param.name == "alarm_value")
				{
					var opt0 = document.createElement("option");
					opt0.innerHTML =  item.zero_name;
					var opt1 = document.createElement("option");
					opt1.innerHTML = item.one_name;
					select.appendChild(opt0);
					select.appendChild(opt1);
				}
				
				if (param.name == "read_write")
				{
						for (var i = 0; i < readWrite.length; i++)
						{
							var opt = document.createElement("option");
							opt.setAttribute("id", readWrite[i]);
							opt.setAttribute("value", i);
							opt.innerHTML = eval("language.Settings_edits_" + readWrite[i]);
							select.appendChild(opt);
						}
				}
				
				if (param.name == "resolution")
				{
					var opt0 = document.createElement("option");
					opt0.innerHTML =  language.Settings_edits_two_digit;
					var opt1 = document.createElement("option");
					opt1.innerHTML = language.Settings_edits_one_digit;
					var opt2 = document.createElement("option");
					opt2.innerHTML = language.Settings_edits_zero_digit;
					select.appendChild(opt0);
					select.appendChild(opt1);
					select.appendChild(opt2);
				}
				
				if (param.name == "apply")
				{
				    for (var k = 0; k < poweronVals.length; k++)
				    {
				        var opt0 = document.createElement("option");
				        opt0.setAttribute("id", poweronVals[k]);
				        opt0.setAttribute("value", k);
				        opt0.innerHTML = eval("language.Settings_edits_apply_" + poweronVals[k]);
				        select.appendChild(opt0);
				    }
				}
				
				select.selectedIndex = parseInt(value);
				td1.appendChild(select);
			}
		}

		tr.appendChild(td1);
	}

	var tdButtons = document.createElement("td");
	tdButtons.setAttribute("class","buttons");
	var edit = document.createElement("input");
	var save = document.createElement("input");
	var cancel = document.createElement("input");
	var del = document.createElement("input");
	edit.setAttribute("type","submit");
	edit.setAttribute("id","edit");
	edit.setAttribute("value",language.Settings_edits_edit);
	edit.setAttribute("data-id","sensor"+item.ionum);
	save.setAttribute("type","submit");
	save.setAttribute("id","save");
	save.setAttribute("data-type",item.type);
	save.setAttribute("value",language.Settings_edits_save);
	save.setAttribute("data-id","sensor"+item.ionum);
	cancel.setAttribute("type","submit");
	cancel.setAttribute("id","cancel");
	cancel.setAttribute("value",language.Settings_edits_cancel);
	cancel.setAttribute("data-id","sensor"+item.ionum);
	del.setAttribute("type","submit");
	del.setAttribute("id","delete");
	del.setAttribute("value",language.Settings_edits_delete);
	del.setAttribute("data-id","sensor"+item.ionum);

	tdButtons.appendChild(del);
	tdButtons.appendChild(edit);
	tdButtons.appendChild(cancel);
	tdButtons.appendChild(save);
	tr.appendChild(tdButtons);

	var page = $("div#settings div#generate li.checkboxs label.selected").attr("id");
	$("div#settings div#generate div#editSensors table").append(tr);

	var sensTypee = DataTypeFromSensType(item.type);

	eval("RefFilesCreate." + sensorTypesToStr2(sensTypee) + "Count = RefFilesCreate." + sensorTypesToStr2(sensTypee) + "Count + 1;");
	RefFilesCreate.totalCount = RefFilesCreate.totalCount + 1;
	if(firstSelect == "") firstSelect = sensorTypesToStr2(sensTypee) + "Name";
};

RefFilesCreate.createEditSensorsList = function()
{
	var devType = $( "div#settings ul#selectform li#type select option:selected" ).attr("value");
	RefFilesCreate.binaryCount = 0; 
	RefFilesCreate.floatCount = 0; 
	RefFilesCreate.outputCount = 0;
	RefFilesCreate.stringCount = 0;
	RefFilesCreate.bitstringCount = 0;
	RefFilesCreate.totalCount = 0;
	firstSelect = "";
	displayStatus = "input";
	$("div#settings div#generate div#Header table").html("");
	$("div#settings div#generate div#editSensors table").html("");
	var items = RefFilesCreate.editsSensorList;
	if (items.length == 0)
	{
		var devType = $( "div#settings ul#selectform li#type select option:selected" ).attr("value");
		RefFilesCreate.activateFields(devType, 1, "input");
		$("input#filename").val("");
		return;
	}

	for ( var i = 0; i < items.length; i++)
	{
		RefFilesCreate.addSensor(items[i]);
	}

	var sensorType = sensorTypes.string;
	if (RefFilesCreate.bitstringCount != 0)
		sensorType = sensorTypes.bitstring;
	if (RefFilesCreate.outputCount != 0)
		sensorType = sensorTypes.output;
	if (RefFilesCreate.floatCount != 0)
		sensorType = sensorTypes.float;
	if (RefFilesCreate.binaryCount != 0)
		sensorType = sensorTypes.binary;
	$("input#filename").val(RefFilesCreate.filename);
	
	RefFilesCreate.activateFields(devType, sensorType, "input");
};

RefFilesCreate.activateFields = function(devType, sensorType, disp)
{	
	$( "ul#selectform" ).css("display", "none");
	$("div#settings div#generate").css("display", "block");

	if (RefFilesCreate.totalCount != 0)
	{
		RefFilesCreate.createEditListHeader(devType, sensorType);
		
		var type = Devices.createSensorType(parseInt(devType), parseInt(sensorType));
		var filter = sensorTypesToStr2(type);
		$("div#settings div#generate tr").css("display", "none");
		$("div#settings div#generate tr td").css("display", "none");
		
		$("div#settings div#generate table tr.head div.switch input").prop("checked", false);
		$("div#settings div#generate table tr.head div.switch input#" + disp).prop("checked", true);
		
		$("div#settings div#generate tr." + filter).css("display", "table-row");
		$("div#settings div#generate tr." + filter + " td." + disp).css("display", "table-cell");
	
		$("div#settings div#generate tr.head").css("display", "table-row");
		$("div#settings div#generate tr.head td." + disp).css("display", "table-cell");
	
		$("div#settings div#generate tr td#name").css("display", "table-cell");
		$("div#settings div#generate tr td.buttons").css("display", "table-cell");
	
		/*if ($("div#settings div#generate div#editSensors table tr.head td[disp='spec']." + filter).length == 0)
		{
			$("div#settings div#generate div#editSensors table tr label#spec").css("display", "none");
			$("div#settings div#generate div#editSensors table tr input#spec").css("display", "none");
			$("div#settings div#generate div#editSensors table tr label").removeClass("spec");
			$("div#settings div#generate div#editSensors table tr input").removeClass("spec");
			$("div#settings div#generate div#editSensors table tr span").removeClass("spec");
		}
		else*/
			$("div#devices div#generate div#Header table td:first-child").addClass("spec");
		$("div#settings div#generate ul.filters li.checkboxs label").css("display", "none");
		$("div#settings div#generate ul.filters li.checkboxs label").removeClass("selected");
		$("div#settings div#generate ul.filters li.checkboxs label b").remove();	
			
		if (RefFilesCreate.binaryCount != 0)
		{
			$("div#settings div#generate ul.filters li.checkboxs label#binaryName").css("display", "inline-block");
			$("div#settings div#generate ul.filters li.checkboxs label#binaryName").append("<b>(" + RefFilesCreate.binaryCount + ")</b>");
		}
		if (RefFilesCreate.floatCount != 0)
		{
			$("div#settings div#generate ul.filters li.checkboxs label#floatName").css("display", "inline-block");
			$("div#settings div#generate ul.filters li.checkboxs label#floatName").append("<b>(" + RefFilesCreate.floatCount + ")</b>");
		}
		if (RefFilesCreate.outputCount != 0)
		{
			$("div#settings div#generate ul.filters li.checkboxs label#outputName").css("display", "inline-block");
			$("div#settings div#generate ul.filters li.checkboxs label#outputName").append("<b>(" + RefFilesCreate.outputCount + ")</b>");
		}
		if (RefFilesCreate.stringCount != 0)
		{
			$("div#settings div#generate ul.filters li.checkboxs label#stringName").css("display", "inline-block");
			$("div#settings div#generate ul.filters li.checkboxs label#stringName").append("<b>(" + RefFilesCreate.stringCount + ")</b>");
		}
		if (RefFilesCreate.bitstringCount != 0)
		{
			$("div#settings div#generate ul.filters li.checkboxs label#bitstringName").css("display", "inline-block");
			$("div#settings div#generate ul.filters li.checkboxs label#bitstringName").append("<b>(" + RefFilesCreate.bitstringCount + ")</b>");
		}
		
		$("div#settings div#generate ul.filters li.checkboxs label#" + sensorTypesToStr2(sensorType) + "Name").addClass("selected");
		
		var childs = $("div#settings div#generate div#editSensors table tr:visible:first td:visible");
		for (var i = 0; i < childs.length; i++)
		{
			var child = $(childs[i]);
			var header = $("div#settings div#generate div#Header table tr td:visible#" + $(childs[i]).attr("id"));
			var  headerWidth= header.css("width");
			var childWidth = child.css("width");
			
			if (parseInt(headerWidth) > parseInt(childWidth))
			{
				child.css("min-width",headerWidth);
				child.css("max-width",headerWidth);
				header.css("min-width",headerWidth);
				header.css("max-width",headerWidth);
			}
			else
			{
				header.css("min-width",childWidth);
				header.css("max-width",childWidth);
				child.css("min-width",childWidth);
				child.css("max-width",childWidth);
			}
		}
	}
	else
	{
		$("div#settings div#generate div#Header table").html("");
		$("div#settings div#generate ul.filters li.checkboxs label").css("display", "none");
	}
};

RefFilesCreate.createEditListHeader = function(devType, sensorType)
{
	if ($("div#settings div#generate div#Header table tr").length != 0)
		return; 
	
	var tr = document.createElement("tr");
	tr.setAttribute("class","head");
	var tdName = document.createElement("td");
	tdName.setAttribute("id", "name");
	var div0 = document.createElement("div");
	div0.setAttribute("class", "switch");

	div0.innerHTML = '<input type="radio" class="switch-input spec" name="view" value="input" id="input" checked>'
				       + '<label for="input" class="switch-label switch-label-on spec" id="input">' + language.Settings_edits_inputname + '</label>'
				       + '<input type="radio" class="switch-input spec" name="view" value="select" id="select">'
				       + '<label for="select" class="switch-label switch-label-off spec" id="select">' + language.Settings_edits_selectname + '</label>'
				       + '<input type="radio" class="switch-input spec" name="view" value="spec" id="spec">'
				       + '<label for="spec" class="switch-label switch-label-on spec" id="spec">' + language.Settings_edits_spec + '</label>'
				       + '<span class="switch-selection spec"></span>'
				     + '</div>';
	tdName.appendChild(div0);
	tr.appendChild(tdName);
	
	
	var type = Devices.createSensorType(parseInt(devType), parseInt(sensorType));
	var filter = sensorTypesToStr(type);
	
	var item = sensorParams[filter];
	for (var i = 0; i < item.length; i++)
	{

		if (item[i].name == "name")
			continue;
		if (item[i].name == "read_period")
			continue;
		if (item[i].name == "read_enable")
			continue;

		var disp = item[i].dispref;
		if (disp == undefined)
			continue;

		var td = document.createElement("td");
		td.innerHTML = eval("language.sensors_" + item[i].name);
		td.setAttribute("id", item[i].name);
		td.setAttribute("class", disp);
		tr.appendChild(td);
	}

	var tdButtons = document.createElement("td");
	tdButtons.setAttribute("class", "buttons");
	tr.appendChild(tdButtons);
	$("div#settings div#generate div#Header table").append(tr);
};

RefFilesCreate.editSensorsValidateClear = function(id)
{
	$("div#settings div#generate div.alert").html("");
	$("div#editSensors table tr td input").removeClass("alert");
};

RefFilesCreate.editSensorsValidate = function(id,type, json)
{
	json.type = type.toString();
	var sensType = sensorTypesToStr2(parseInt(type));
	var elements = $("div#editSensors table tr#" + id + " td input, \
	div#editSensors table tr#" + id + " td select");
	var page = $("li.checkboxs label.selected").attr("id");
	var alertHtml = "";
			
	$("div#settings div#generate div.alert").html("");
	var readF = undefined;
	var writeF = undefined;
	
	var isvalid = true;
    for (var i = 0; i < elements.length; i++)
    {
        var element 	= $(elements[i]);
        var dataType 	= element.attr("data-type");
        var dataName    = element.attr("data-name");
        var dflt = element.attr("default");
        var minval 		= element.attr("minval");
        var maxval 		= element.attr("maxval");
        var jsON = {
            type: dataType,
            min: minval,
            max: maxval
        };
        
        if (element.is("input") && element.attr("type") != "checkbox")
        {
            if (dataName == "read_function_code")
                readF = true;
            if (dataName == "write_function_code")
                writeF = true;
        		
			var value = element.val();
			
			if(value.length == 0)
			{
	        	if (dataName == "read_function_code")
	        		readF = false;
	        	if (dataName == "write_function_code")
	        		writeF = false;
        		
				if (dflt != undefined)
				{
					json[dataName] = dflt;
					continue;
				}			
			}

			jsON.val = value;
			jsON.ismandatory = "true";
			jsON.obj  		 = element;
			jsON.objfocus    = element;
			jsON.page        = $("div#settings div#generate div.alert");
			jsON.errprefix   = eval("language.sensors_" + dataName) + ":";
			
			//validation of element
			if(!Validator.validate(jsON))
				isvalid = false;
			else if(isvalid)
				json[dataName] = value;
			
        }
		else if (element.is("select"))
		{
			json[dataName] = element.prop("selectedIndex").toString();
		}
		else if (element.is("input") && element.attr("type") == "checkbox")
		{
			json[dataName] = element.prop("checked") ? "1":"0";
		}
	}
	
	if (isvalid)
	{
		if (isFloat(type))
		{
			var alarmLow = parseFloat($("div#editSensors table tr#" + id + " td input[data-name='alarm_low']").val());
			var warningLow = parseFloat($("div#editSensors table tr#" + id + " td input[data-name='warning_low']").val());
			var warningHigh = parseFloat($("div#editSensors table tr#" + id + " td input[data-name='warning_high']").val());
			var alarmHigh = parseFloat($("div#editSensors table tr#" + id + " td input[data-name='alarm_high']").val());
			
			if (!((alarmLow <= warningLow) && (warningLow <= warningHigh) && (warningHigh <= alarmHigh) && (alarmLow <= warningLow)))
			{
				$("div#editSensors table tr#" + id + " td input[data-name='alarm_low']").addClass("alert");
				$("div#editSensors table tr#" + id + " td input[data-name='warning_low']").addClass("alert");
				$("div#editSensors table tr#" + id + " td input[data-name='warning_high']").addClass("alert");
				$("div#editSensors table tr#" + id + " td input[data-name='alarm_high']").addClass("alert");
				alertHtml = $("div#settings div#generate div.alert").html() + "<br />" +  language.Settings_edits_notvalidlimits;
				$("div#settings div#generate div.alert").html(alertHtml);
				isvalid = false;
			}	
		}
		
	   	if (readF != undefined && writeF != undefined)
	   	{
	   		if (readF && writeF)
	   			json["read_write"] = "2";
	   		else if (readF)
	   			json["read_write"] = "0";
	   		else if (writeF)
	   			json["read_write"] = "1";
	   		else
	   		{
	   			$("div#editSensors table tr#" + id + " td input[data-name='read_function_code']").addClass("alert");
	   			$("div#editSensors table tr#" + id + " td input[data-name='write_function_code']").addClass("alert");
	   			alertHtml = $("div#settings div#generate div.alert").html() + "<br />" +  language.Settings_edits_read_write_function_code_define;
				$("div#settings div#generate div.alert").html(alertHtml);
				isvalid = false;
	   		}
	   	}
	}
	
	return isvalid;
};

RefFilesCreate.uploadFromComp = function(image, file)
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/systemsettings.cgi?addRefFileFromComp" + "&filename=" + file.name;
	$.ajax({
	    url : url,
	    type: "POST",
	    data: image,
        contentType: 'application/octet-stream',
        dataType: "text",
	    success: function(Data, textStatus, jqXHR)
	    {
	    	Main.unloading();
	    	Json = jQuery.parseJSON(Data);
	    	if (Json["return"] != "true")
	    		Main.alert(language.reffileupdatefromcomp_failed + ": " + Json["return"]);
	    	else
	    		Main.alert(language.reffileupdatefromcomp_success);
	    		

		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};
RefFilesCreate.validateAll = function(dataLst)
{
	var list = $("div#generate  div#editSensors tr:not(.head) input#save");
	for(var i = 0; i < list.length; i++)
	{
		var item = $(list[i]);
			var id = item.data("id");
			var type = item.data("type");

			ionum = (i + 1).toString();

			var json = {ionum:ionum};
			var ret= RefFilesCreate.editSensorsValidate(id,type, json);
			if (ret == false)
			{
				return false;
			}
			dataLst.push(json);
	}
	return true;
};

RefFilesCreate.addReffile = function()
{
	Main.loading();
	var filename = $("input#filename").val();
	if (filename.length == 0)
	{
		Main.alert(language.filename_empty);
		Main.unloading();
		return;
	}
	var dataLst = [];
	if (RefFilesCreate.validateAll(dataLst) == false)
	{
		Main.unloading();
		return;
	}
	var deviceType = $("div#settings ul#selectform li#type select option:selected").attr("value");
	var url = "http://" + hostName + "/cgi-bin/megweb.cgi?addReffile&device_type=" + deviceType + "&filename=" + filename;
	json = JSON.stringify(dataLst);
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType: 'application/octet-stream',
	    success: function(data, textStatus, jqXHR)
	    {
	    	Main.alert(language.reffile_add_success);
	    	var opt = document.createElement("option");
	    	opt.setAttribute("value", filename);
	    	opt.setAttribute("loc", "m");
	    	opt.innerHTML = filename;
	    	$("div#settings ul#selectform li#reffiles select").append(opt);
	    	$("div#settings ul#selectform").css("display", "block");
	    	$("div#settings div#generate").css("display", "none");
			Main.unloading();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.reffile_add_fail + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

RefFilesCreate.updateReffile = function()
{
	Main.loading();
	var filename = $("input#filename").val();
	if (filename.length == 0)
	{
		Main.alert(language.filename_empty);
		Main.unloading();
		return;
	}
	var dataLst = [];
	if (RefFilesCreate.validateAll(dataLst) == false)
	{
		Main.unloading();
		return;
	}
	var deviceType = $("div#settings ul#selectform li#type select option:selected").attr("value");
	var url = "http://" + hostName + "/cgi-bin/megweb.cgi?updateReffile&device_type=" + deviceType + "&filename=" + RefFilesCreate.filename + "&new-filename=" + filename;
	json = JSON.stringify(dataLst);
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType: 'application/octet-stream',
	    success: function(data, textStatus, jqXHR)
	    {
	    	Main.alert(language.reffile_update_success);
	    	if (RefFilesCreate.filename != filename)
	    	{
		    	$("div#settings ul#selectform li#reffiles select option[value='"+ RefFilesCreate.filename+ "']").attr("value", filename);
		    	$("div#settings ul#selectform li#reffiles select option[value='"+ filename+ "']").attr("loc", "m");
		    	$("div#settings ul#selectform li#reffiles select option[value='"+ filename+ "']").html(filename);
	    	}
	    	$("div#settings ul#selectform").css("display", "block");
	    	$("div#settings div#generate").css("display", "none");

			Main.unloading();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.reffile_update_fail + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

$(function() {

	$("div#settings").on("change", "ul#selectform li#type select", function() {
		RefFilesCreate.getRefFiles();
	});

	$("div#settings").on("click", "ul#selectform li#buttons input#buttonAdd", function() {
		RefFilesCreate.ionum = 0;
		RefFilesCreate.formType = "add";
		RefFilesCreate.editsSensorList = [];
		RefFilesCreate.createEditSensorsList();
	});

	$("div#settings").on("click", "ul#selectform li#buttons input#buttonAddRef", function()
	{
		$("#buttonAddRefFile").click();
	});

	$("div#settings").on("change", "#buttonAddRefFile", function()
	{
		var file = $("#buttonAddRefFile")[0].files[0]; //Files[0] = 1st file
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = shipOff;

		function shipOff(event) {
		    var result = event.target.result;
		    var fileName = $("#buttonAddRefFile")[0].files[0].name; //Should be 'picture.jpg'
		    RefFilesCreate.uploadFromComp(result, file);
		}
	});

	$("div#settings").on("click", "ul#selectform li#buttons input#buttonUpdate", function() {
		RefFilesCreate.formType = "update";
		RefFilesCreate.getRefFile();
	});

	$("div#settings").on( "click", "div#generate div#editSensors table tr td input#save", function() {
		var url = "updateSensor" + $(this).data("url");
		var id = $(this).data("id");
		var type = $(this).data("type");
		var json = {};
		var ret= RefFilesCreate.editSensorsValidate(id,type, json);
		if (ret)
		{		
			Settings.editSensorsValidateClear("all");
	
	    	if(id != undefined) {
				$("tr#" + id + " td input#edit").css("display","inline-block");
				$("tr#" + id + " td input#delete").css("display","inline-block");
				$("tr#" + id + " td input#save").css("display","none");
				$("tr#" + id + " td input#cancel").css("display","none");
	
				$("tr#" + id + " input[type='text']").each(function() {
					$(this).prop('disabled', true);
				});
	
				$("tr#" + id + " select").each(function() {
					$(this).prop('disabled', true);
				});			
	
				$("tr#" + id + " input[type='checkbox']").each(function() {
					$(this).prop('disabled', true);
				});
	
			} else {
				$("div#editSensors tr td input#edit").css("display","inline-block");
				$("div#editSensors tr td input#delete").css("display","inline-block");
				$("div#editSensors tr td input#save").css("display","none");
				$("div#editSensors tr td input#cancel").css("display","none");
	
				$("div#editSensors tr input[type='text']").each(function() {
					$(this).prop('disabled', true);
				});
	
				$("div#editSensors tr select").each(function() {
					$(this).prop('disabled', true);
				});				
	
				$("div#editSensors tr input[type='checkbox']").each(function() {
					$(this).prop('disabled', true);
				});
			}
		}
	});

	$("div#settings").on( "click", "div#generate div#editSensors table tr td input#edit", function() {
		var id = $(this).attr("data-id");
		var status = false;
		$(this).addClass("editt");

		allSaved.push(id);

		$("div#generate  tr#" + id + " td input#edit").css("display","none");
		$("div#generate  tr#" + id + " td input#delete").css("display","none");
		$("div#generate  tr#" + id + " td input#save").css("display","inline-block");
		$("div#generate  tr#" + id + " td input#cancel").css("display","inline-block");

		$("div#generate  div#editSensors table tr#" + id + " input[type='text']").each(function() {
			$(this).prop('disabled', status);
			var val = $(this).val();
			$(this).attr("data-val", val);
		});

		$("div#generate div#editSensors table tr#" + id + " input[type='checkbox']").each(function() {
			$(this).prop('disabled', status);
			var val = $(this).val();
			$(this).attr("data-val", val);
		});

		$("div#generate div#editSensors table tr#" + id + " select").each(function() {
			$(this).prop('disabled', status);
			var val = $(this).val();
			$(this).attr("data-val", val);
		});
	});

	$("div#settings").on( "click", "div#generate  div#editSensors table tr td input#cancel", function() {
		var id = $(this).data("id");
		RefFilesCreate.editSensorsValidateClear(id);
		$($(this).parent().children("input#edit")[0]).removeClass("editt");
		var status = true;
		var index = allSaved.indexOf(id);
		allSaved.splice(index,1);

		$("div#generate div#editSensors table tr#" + id + " td input#edit").css("display","inline-block");
		$("div#generate div#editSensors table tr#" + id + " td input#delete").css("display","inline-block");
		$("div#generate div#editSensors table tr#" + id + " td input#save").css("display","none");
		$("div#generate div#editSensors table tr#" + id + " td input#cancel").css("display","none");

		$("div#generate  div#editSensors table tr#" + id + " input[type='text']").each(function() {
			$(this).prop('disabled', status);
			var val = $(this).attr("data-val");
			$(this).val(val);
		});

		$("div#generate  div#editSensors table tr#" + id + " input[type='checkbox']").each(function() {
			$(this).prop('disabled', status);
			var val = parseInt($(this).attr("data-val"));
			$(this).val(val);
			if(val == 0) {
				$(this).prop('checked', false);
			} else {
				$(this).prop('checked', true);
			}
		});

		$("div#generate div#editSensors table tr#" + id + " select").each(function() {
			$(this).prop('disabled', status);
			var dataval = parseInt($(this).attr("data-val"));
			$(this).find("option").each(function(){
				var val = parseInt($(this).val());
				if(val == dataval) {
					$( this ).prop('selected', true);
				} else {
					$( this ).prop('selected', false);
				}
			});
		});
	});

	$("div#settings").on( "click", "div#generate input#saveall", function() {
		var refs = $("ul#selectform li#reffiles select option[value='" + $("input#filename").val() + "']");
		if (RefFilesCreate.formType == "add")
		{		
			if (refs.length != 0)
			{
				Main.alert(language.Settings_reffile_name_conflict0);
			}
			else
				RefFilesCreate.addReffile();
		}
		else if (RefFilesCreate.formType == "update")
		{	
			if (refs.length != 0 && $(refs[0]).attr("loc") == "e")
			{
				Main.alert(language.Settings_reffile_name_conflict1);
			}
			else
				RefFilesCreate.updateReffile();	
		}
	});

	$("div#settings").on( "click", "div#generate table tr.head div.switch input", function() {
		if(displayStatus != $(this).val()) {
			displayStatus = $(this).val();
			var devType = $( "ul#selectform li#type select option:selected" ).attr("value");
			var sensType = $("div#settings li.checkboxs label.selected").attr("for");
			var sensorType = sensorTypes[sensType];
			RefFilesCreate.activateFields(devType, sensorType, displayStatus);
		}
	});

	$("div#settings").on( "click", "div#generate ul.filters li label", function() {
		//Main.loading();
		if(!$(this).hasClass("selected")) {
			$("div#generate ul.filters li label").removeClass("selected");
			$(this).addClass("selected");
			var id = $(this).attr("for");
			var devType = $( "ul#selectform li#type select option:selected" ).attr("value");
			var sensorType = sensorTypes[id];
			firstSelect = id + "Name";
			$("div#settings div#generate div#Header table").html("");
			RefFilesCreate.activateFields(devType, sensorType, displayStatus);
		}
	});
	
	$("div#settings").on( "click", "div#generate input#Cancel", function() {
	    	$("div#settings ul#selectform").css("display", "block");
	    	$("div#settings div#generate").css("display", "none");
	});

	$("div#settings").on( "click", "div#generate div#editSensors table tr td input#delete", function() {
		$("div#overlay").css("display","block");
		$("div#settings div#alertRemove").css("display","block");
		$("div#settings div#alertRemove input#buttonRemove").val(language.remove);
		$("div#settings div#alertRemove input#buttonRemove").attr("remove", $(this).attr("data-id"));
		$("div#settings div#alertRemove input#buttonRemove").addClass("sensorDelete");
		$("div#settings div#alertRemove input#buttonClose").val(language.close);
		$("div#settings div#alertRemove span.text").html(language.Settings_delete_sensor);	
	});

	$("div#settings").on( "click", "div#alertRemove input#buttonClose", function() {
		$("div#overlay").css("display","none");
		$("div#settings div#alertRemove").css("display","none");
	});

	$("div#settings").on( "click", "div#alertRemove input#buttonRemove", function() {
		var tr = $("div#generate div#editSensors table tr#" + $(this).attr("remove"));
		var sensType = strToSensorType(tr.attr("class"));
		sensType = DataTypeFromSensType(sensType);
		var sensorType = sensorTypesToStr2(sensType);
		eval("RefFilesCreate." + sensorType + "Count = RefFilesCreate." + sensorType + "Count - 1;");
		RefFilesCreate.totalCount = RefFilesCreate.totalCount - 1;
		tr.remove();
		$("div#overlay").css("display","none");
		$("div#settings div#alertRemove").css("display","none");
		$("div#settings div#alertRemove input#buttonRemove").removeClass("sensorDelete");
		if ($("div#generate table tr:not(.head)").length == 0)
		{
				$("div#generate table tr.head").remove();
		}
		var devType = $( "div#settings ul#selectform li#type select option:selected" ).attr("value");
		var sensTypee = 1;
		if (eval("RefFilesCreate." + sensorType + "Count") != 0)
			sensTypee = sensType;
		else
		{
			if (RefFilesCreate.floatCount != 0)
			{
				sensTypee = sensorTypes.float;
				firstSelect = "floatName";	
			}
			else if (RefFilesCreate.binaryCount != 0)
			{
					sensTypee = sensorTypes.binary;
				firstSelect = "binaryName";	
			}
			else if (RefFilesCreate.outputCount != 0)
			{
				sensTypee = sensorTypes.output;
				firstSelect = "outputName";	
			}
			else if (RefFilesCreate.stringCount != 0)
			{
				sensTypee = sensorTypes.string;
				firstSelect = "stringName";	
			}
			else if (RefFilesCreate.bitstringCount != 0)
			{
				sensTypee = sensorTypes.bitstring;
				firstSelect = "bitstringName";	
			}
			
			$("div#settings div#generate div#Header table").html("");
		}
		
		RefFilesCreate.activateFields(devType, sensTypee, displayStatus);
	});	

	$("div#settings").on( "click", "div#generate  input#newsensor", function() {
		SelectType = undefined;
		var devType = parseInt($("ul#selectform li#type option:selected").attr("value"));
		var sensorType = Devices.createSensorType(devType, sensorTypes.float);
		var html = language.Settings_edits_addnewsensor;
		$("div#editWindow td#modulsensorname").html(html);
		$("div#editWindow tr#trtype select").prop("selectedIndex", 0);
		$("div#editWindow td.alert").html("");
		$("div#overlay").css("display","block");
		$("div#editWindow td.alert").html("");
		$("div#editWindow").css("display","block");
		$("div#editWindow input#buttonUpdate").val(language.save);
		$("div#editWindow input#buttonCancel").val(language.cancel);	
		$("div#editWindow").attr("window-type", "add-reffile");
		$("div#editWindow").attr("object", "sensors");
		$("div#editWindow").attr("object-type", sensorTypesToStr(sensorType));	
		Devices.createEditWindow();
		Devices.DeviceFieldsToDefaults();
	});
});