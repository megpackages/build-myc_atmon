Settings.ftpData = {};

Settings.callFtp = function() {	
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<ul class="element" id="ftpform" style="display:none;">'
				+	'<li id="ftpsize"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="ftpport"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="anonim"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="username"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="password"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="buttons"><div class="left"></div><div class="right"></div></li>'
				+ '</ul>';
	$("div#settings div.form").html(html);
	Settings.getFTP();
	//Settings.createFtpForm();
};

Settings.getFTP = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getFtpServer");	 
	data.success(function() {
		Settings.ftpData = jQuery.parseJSON(data.responseText);
		Settings.createFtpForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.postFTP = function() {
	Main.loading();
	json = JSON.stringify(Settings.ftpData);

	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?updateFtpServer";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			//Settings.callSmtp();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.createFtpForm = function() {	
	Main.unloading();
	var item = Settings.ftpData;
	var size = item.ftp_server_dir_size;
	var port = item.ftp_server_port;
	var anonim = item.ftp_anonymous_login;
	var username = item.ftp_username;
	var password = item.ftp_passwd;

	var ftpInput = document.createElement("input");
	ftpInput.setAttribute("type","text");
	ftpInput.setAttribute("id","ftpInput");
	ftpInput.setAttribute("value",size);
	$("li#ftpsize div.left").html(language.Settings_smtp_ftp_size);
	$("li#ftpsize div.right").append(ftpInput);

	var portInput = document.createElement("input");
	portInput.setAttribute("type","text");
	portInput.setAttribute("id","portInput");
	portInput.setAttribute("value",port);
	$("li#ftpport div.left").html(language.Settings_smtp_ftp_port);
	$("li#ftpport div.right").append(portInput);

	var anonimInput = document.createElement("input");
	anonimInput.setAttribute("type","checkbox");
	anonimInput.setAttribute("id","anonimInput");
	if(anonim == 1) {
		anonimInput.checked = true;
	}
	$("li#anonim div.left").html(language.Settings_smtp_ftp_anonim);
	$("li#anonim div.right").append(anonimInput);

	var usernameInput = document.createElement("input");
	usernameInput.setAttribute("type","text");
	usernameInput.setAttribute("id","usernameInput");
	usernameInput.setAttribute("value",username);
	$("li#username div.left").html(language.Settings_smtp_ftp_username);
	$("li#username div.right").append(usernameInput);


	var passwordInput = document.createElement("input");
	passwordInput.setAttribute("type","password");
	passwordInput.setAttribute("id","passwordInput");
	//passwordInput.setAttribute("value",password);
	$("li#password div.left").html(language.Settings_smtp_ftp_password);
	$("li#password div.right").append(passwordInput);

	var saveInput = document.createElement("input");
	saveInput.setAttribute("type","submit");
	saveInput.setAttribute("id","save");
	saveInput.setAttribute("value", language.Settings_ftp_save);
	$("li#buttons div.left").html("");
	$("li#buttons div.right").append(saveInput);

	$("div#settings ul.element").css("display","block");
};

Settings.validateFtpForm = function(page) {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#ftpsize").removeClass("alert");
	$("li#ftpport").removeClass("alert");
	$("li#anonim").removeClass("alert");
	$("li#username").removeClass("alert");
	$("li#password").removeClass("alert");

	var size     = $("li#ftpsize input").val();
	var port     = $("li#ftpport input").val();
	var anonim   = $("li#anonim input").prop("checked");
	var username = $("li#username input").val();
	var password = $("li#password input").val();

	var isvalid = true;

	//validation of size
	if(!Validator.validate( { "val" : size, "type" : "i", "ismandatory" : "true", "obj" : $("li#ftpsize"), "objfocus" : $("li#ftpsize input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	//validation of port
	if(!Validator.validate( { "val" : port, "type" : "port", "ismandatory" : "true", "obj" : $("li#ftpport"), "objfocus" : $("li#ftpport input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	//validation of username
	if(!Validator.validate( { "val" : username, "type" : "ftpusername", "ismandatory" : "false", "obj" : $("li#username"), "objfocus" : $("li#username input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	//validation of password
	if(!Validator.validate( { "val" : password, "type" : "ftppassword", "ismandatory" : "false", "obj" : $("li#password"), "objfocus" : $("li#password input"), "page" : $("div#settings div.alert")}))
		isvalid = false;		

	if(isvalid) {

		if(anonim) {
			anonim = "1";
		} else {
			anonim = "0";
		}

		Settings.ftpData = {
			ftp_server_dir_size: size,
			ftp_server_port: port,
			ftp_anonymous_login: anonim,
			ftp_username: username,
			ftp_passwd: password
		};
	}

	return isvalid;
};

$(function() {
	$("div#settings").on( "click", "ul#ftpform input#save", function() {
		if(Settings.validateFtpForm()) {
			Settings.postFTP();
		}
	});
});