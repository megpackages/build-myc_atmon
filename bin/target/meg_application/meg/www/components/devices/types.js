
access =
{
	r : 0,
	w : 1,
	rw  : 2
}


////////////////////////// CONNECTORS //////////////////////////


connectorTypes = 
{
	none:{value:0, addable:false},
	can:{value:1, addable:false},
	proc:{value:2, addable:false},
	ipv4:{value:3, addable:false}
}

var	base = [
//	    { name: "objectid", access: access.r, type: "s", html:"text"},
	    { name: "hwaddr", access: access.r, type: "s", html:"text"},
	    { name: "type", access: access.r, type: "i", html:"text"},
	    { name: "transport", access: access.r, type: "i", html:"text"},
	    { name: "version", access: access.r, type: "s", html:"text"},
	    { name: "active", access: access.r, type: "s", html:"text"},
	    { name: "description", access: access.rw, type: "s", html:"text"},
	    { name: "ping_period", access: access.rw, type: "i", html:"text", min:"20", max:"600"}
		];
var	_can = [
	    { name: "canid", access: access.r, type: "i", html:"text"}
	];
var	_proc = [
	    { name: "process", access: access.r, type: "s", html:"text"}
	];
var	_ipv4 = [
	    { name: "hostaddr", access: access.r, type: "s", html:"text"},
	    { name: "hostport", access: access.r, type: "s", html:"text"}
	];
connectorParams = 
{
	can: base.concat(_can),
	proc: base.concat(_proc),
	ipv4: base.concat(_ipv4)
}

//////////////////////// END OF CONNECTORS /////////////////////





////////////////////////// PORTS ///////////////////////////////

Baudrates = 
[
	1200,
	2400,
	4800,
	9600,
	19200,
	38400,
	57600,
	115200
];

Databits = 
[
	5,
	6,
	7,
	8
];

Stopbits = 
[
	1,
	2
];

Parities = 
[
	"none",
	"odd",
	"even"
];


portTypes = 
{
	none:{value:0, addable:false},
	internal:{value:1, addable:false},
	serial:{value:2, addable:false},
	extendable:{value:3, addable:false}
}


var	base = [
	    { name: "objectid", access: access.r, type: "s", html:"text"},
	  	{ name: "number", access: access.r, type: "i", html:"text"},
	    { name: "type", access: access.r, type: "i", html:"text"}
		];

var	_serial = [
	    { name: "boudrate", access: access.rw, type: "i", html:"select", func:"createBaudrateSelect();"},
	    { name: "databits", access: access.rw, type: "i", html:"select", func:"createDatabitsSelect();"},
	    { name: "stopbits", access: access.rw, type: "i", html:"select", func:"createStopbitsSelect();"},
	    { name: "parity", access: access.rw, type: "i", html:"select", func:"createParitySelect();"}
	];
var	_extendable = [
	];

portParams = 
{
	internal: base,
	serial: base.concat(_serial),
	extendable: base.concat(_extendable)
}

//////////////////////// END OF PORTS //////////////////////////




////////////////////////// DEVICES /////////////////////////////

var	base =  [
	    { name: "objectid", access: access.r, type: "s", html:"text"},
	    { name: "name", access: access.rw, type: "s", html:"text"},
	    { name: "addr", access: access.w, type: "s", html:"text"},
	    { name: "request_timeout", access: access.rw, type: "i", html:"text" ,default: "25", min:"10", max:"60"},
	    { name: "read_period", access: access.rw, type: "i", html:"text" ,default: "25", min:"5"}
	];

var	baseup =  [
    { name: "objectid", access: access.r, type: "s", html:"text"},
    { name: "name", access: access.rw, type: "s", html:"text"},
    { name: "addr", access: access.rw, type: "s", html:"text"},
    { name: "request_timeout", access: access.rw, type: "i", html:"text" ,default: "25", min:"10", max:"60"},
	{ name: "read_period", access: access.rw, type: "i", html:"text" ,default: "25", min:"5"}
];

var	_modbus_rtu = [
	    { name: "ref_file", access: access.w, type: "s", html:"select",func:"getRefFiles();"}
	];

var	_modbus_ascii = [
	    { name: "ref_file", access: access.w, type: "s", html:"select",func:"getRefFiles();"}
	];

var	_modbus_tcp = [
	    { name: "ip", access: access.rw, type: "ip", html:"text"},
	    { name: "port", access: access.rw, type: "i", html:"text", max:"65535", min:"1"},
	    { name: "ref_file", access: access.w, type: "s", html:"select",func:"getRefFiles();"}
	];

var	_meter = [
	    { name: "ref_file", access: access.w, type: "s", html:"select",func:"getRefFiles();"}
	];

var	_transparent = [
	    { name: "plugin_path", access: access.r, type: "s", html:"text"},
	    { name: "ref_file", access: access.w, type: "s", html:"select",func:"getRefFiles();"}
	];

var	_snmp = [
	    { name: "port", access: access.rw, type: "i", html:"text", max:"65535", min:"1", default:"161"},
	    { name: "version", access: access.rw, type: "i", html:"select", func:"createSnmpVersionSelect();"},
	    { name: "community_name", access: access.rw, type: "s", html:"text"},
	    { name: "security_name", access: access.rw, type: "s", html:"text", default:""},
	    { name: "password", access: access.rw, type: "s", html:"password", default:""},
	    { name: "ref_file", access: access.w, type: "s", html:"select",func:"getRefFiles();"}
	];

var	_iec61850 = [
	    { name: "port", access: access.rw, type: "i", html:"text", max:"65535", min:"1", default: "102"},
	    //{ name: "domain", access: access.rw, type: "regexpattern", html:"text"},
	    { name: "dataset_location", access: access.rw, type: "regexpattern", html:"text", default: ""},
	    //{ name: "rcb", access: access.rw, type: "regexpattern", html:"text"},
	    { name: "ref_file", access: access.w, type: "s", html:"select",func:"getRefFiles();"}
	];

var	_ping = [];

var	_telnet = [];

var	_pnp = [
	    //{ name: "id", access: access.r, type: "i", html:"text"},
	    { name: "hwaddr", access: access.r, type: "s", html:"text"}
	];

var _virtual = [];

var _infrared = [
        { name: "plugin_path", access: access.r, type: "s", html:"text"},
        { name: "ref_file", access: access.w, type: "s", html:"select", func:"getRefFiles();"}
    ];
    
var _pcomm = [
        { name: "ref_file", access: access.w, type: "s", html:"select", func:"getRefFiles();"}
    ];

addrTypes = {
	modbus_rtu: {type: "i", max: "1024", min: "1" },
	modbus_tcp: {type: "i", max: "1024", min: "1" },
	modbus_ascii: {type: "i", max: "1024", min: "1" },
	meter: {type: "meterserialno"},
	transparent: {type: "i"},
	snmp: {type: "ip"},
	iec61850: {type: "ip"},
    infrared: {type: "i", max: "1024", min: "1" },
    pcomm: {type: "i", max: "1024", min: "1" }
};

Reffiles = 
{
	"modbus_rtu":"Modbus",
	"modbus_ascii":"Modbus",
	"modbus_tcp":"Modbus",
	"meter":"Meter",
	"transparent":"Transparent",
	"snmp":"SNMP",
	"iec61850": "Iec61850",
	"infrared" : "Infrared",
	"pcomm": "Pcomm"
};

snmpVersions = 
{
	version:["snmpv1", "snmpv2c", "snmpv3"]
};


deviceTypesToStr0 = function(type)
{
	for (k in deviceTypes)
	{
		if (deviceTypes[k].value == parseInt(type))
		{
			return k;
		}
	}
};

deviceTypes = 
{
	internal:{value:1, addable:false, sensorAddable:false,reffileneeded:false},
	modbus_rtu:{value:2, addable:true, sensorAddable:true,reffileneeded:true},
	modbus_ascii:{value:3, addable:true, sensorAddable:true,reffileneeded:true},
	modbus_tcp:{value:4, addable:true, sensorAddable:true,reffileneeded:true},
	meter:{value:5, addable:true, sensorAddable:true,reffileneeded:true},
	transparent:{value:6, addable:true, sensorAddable:true,reffileneeded:true},
	snmp:{value:7, addable:true, sensorAddable:true,reffileneeded:true},
	iec61850:{value:8, addable:true, sensorAddable:true,reffileneeded:true},
	ping:{value:9, addable:false, sensorAddable:true,reffileneeded:false},
	telnet:{value:10, addable:false, sensorAddable:true,reffileneeded:false},
	pnp:{value:11, addable:false, sensorAddable:false,reffileneeded:false},
	virtual:{value:12, addable:false, sensorAddable:true, reffileneeded:false},
    infrared:{value:13, addable:true, sensorAddable:false, reffileneeded:true},
    pcomm:{value:14, addable:true, sensorAddable:true, reffileneeded:true}
};


deviceStatus = 
{
	running: 0,
	suspended: 1,
	lost: 2
};

deviceTypesToStr = function(type)
{
	for (k in deviceTypes)
	{
		if (deviceTypes[k].value == parseInt(type))
		{
			if (type == deviceTypes["modbus_tcp"].value || type == deviceTypes["modbus_rtu"].value || type == deviceTypes["modbus_ascii"].value)
				return "modbus";
			else
				return k;
		}
	}
};

deviceParams = 
{
	internal : base,
	modbus_rtu: baseup.concat(_modbus_rtu),
	modbus_tcp: baseup.concat(_modbus_tcp),
	modbus_ascii: baseup.concat(_modbus_ascii),
	meter: baseup.concat(_meter),
	transparent: base.concat(_transparent),
	snmp: baseup.concat(_snmp),
	iec61850: baseup.concat(_iec61850),
	ping: base.concat(_ping),
	telnet: base.concat(_telnet),
	pnp: base.concat(_pnp),
    virtual: base.concat(_virtual),
    infrared: base.concat(_infrared),
    pcomm: base.concat(_pcomm)
};

//////////////////////// END OF DEVICES ////////////////////////


//////////////////////// ADD DEVICE ////////////////////////////

ADDEVICE = [
	{ name: "type", access: access.w, type: "i", html:"select", func: "createSelectDeviceType();"},
    { name: "hwaddr", access: access.w, type: "s", html:"select", func: "getConnectorsForAddDevice();"},
    { name: "number", access: access.w, type: "s", html:"select", func: "nop();"},
];

////////////////////////////////////////////////////////////////

///////////////////////// MOVE DEVICE /////////////////////////


MOVEDEVICE = [
	{ name: "connector_from", access: access.rw, type: "s", html:"select", func:"getMovableConnectors();"},
	{ name: "port_from", access: access.rw, type: "s", html:"select", func:"nop();"},
	{ name: "movable", access: access.rw, type: "s", html:"select", func:"nop();"},
	{ name: "connector_to", access: access.rw, type: "s", html:"select", func:"nop();"},
	{ name: "port_to", access: access.rw, type: "s", html:"select", func:"nop();"}
];

///////////////////////////////////////////////////////////////



////////////////////////// SENSORS /////////////////////////////

sensorTips = 
{
	//none: {value:0},
	float:{value:1},
	binary:{value:2},
	output:{value:3},
	string:{value:4},
	bitstring:{value:5},
	ieee_754:{value:6}
};

deviceTips =
{
	modbus: {value:2},
	meter: {value:5},
	transparent: {value:6},
	snmp: {value:7},
	iec61850: {value:8},
	ping: {value:9},
	telnet: {value:10},
	pnp: {value:11},
    virtual: {value:12},
    infrared: {value:13},
    pcomm: {value:14}
};

GenerateSensorType = function(deviceType, sensorType)
{
	return ((deviceType << 16) | sensorType);
}

sensorTypes = 
{
    none: 0, 
    float: sensorTips.float.value, 
    binary: sensorTips.binary.value, 
    output: sensorTips.output.value, 
    string: sensorTips.string.value, 
    bitstring: sensorTips.bitstring.value, 
    ieee_754: sensorTips.ieee_754.value, 
    float_modbus: GenerateSensorType(deviceTips.modbus.value, sensorTips.float.value), 
    binary_modbus: GenerateSensorType(deviceTips.modbus.value, sensorTips.binary.value), 
    output_modbus: GenerateSensorType(deviceTips.modbus.value, sensorTips.output.value), 
    string_modbus: GenerateSensorType(deviceTips.modbus.value, sensorTips.string.value), 
    bitstring_modbus: GenerateSensorType(deviceTips.modbus.value, sensorTips.bitstring.value), 
    ieee_754_modbus: GenerateSensorType(deviceTips.modbus.value, sensorTips.ieee_754.value),
    float_meter: GenerateSensorType(deviceTips.meter.value, sensorTips.float.value), 
    binary_meter: GenerateSensorType(deviceTips.meter.value, sensorTips.binary.value), 
    output_meter: GenerateSensorType(deviceTips.meter.value, sensorTips.output.value), 
    string_meter: GenerateSensorType(deviceTips.meter.value, sensorTips.string.value), 
    bitstring_meter: GenerateSensorType(deviceTips.meter.value, sensorTips.bitstring.value), 
    ieee_754_meter: GenerateSensorType(deviceTips.meter.value, sensorTips.ieee_754.value), 
    float_transparent: GenerateSensorType(deviceTips.transparent.value, sensorTips.float.value), 
    binary_transparent: GenerateSensorType(deviceTips.transparent.value, sensorTips.binary.value), 
    output_transparent: GenerateSensorType(deviceTips.transparent.value, sensorTips.output.value), 
    string_transparent: GenerateSensorType(deviceTips.transparent.value, sensorTips.string.value), 
    bitstring_transparent: GenerateSensorType(deviceTips.transparent.value, sensorTips.bitstring.value), 
    ieee_754_transparent: GenerateSensorType(deviceTips.transparent.value, sensorTips.ieee_754.value),
    float_snmp: GenerateSensorType(deviceTips.snmp.value, sensorTips.float.value), 
    binary_snmp: GenerateSensorType(deviceTips.snmp.value, sensorTips.binary.value), 
    output_snmp: GenerateSensorType(deviceTips.snmp.value, sensorTips.output.value), 
    string_snmp: GenerateSensorType(deviceTips.snmp.value, sensorTips.string.value), 
    bitstring_snmp: GenerateSensorType(deviceTips.snmp.value, sensorTips.bitstring.value), 
    ieee_754_snmp: GenerateSensorType(deviceTips.snmp.value, sensorTips.ieee_754.value), 
    float_iec61850: GenerateSensorType(deviceTips.iec61850.value, sensorTips.float.value), 
    binary_iec61850: GenerateSensorType(deviceTips.iec61850.value, sensorTips.binary.value), 
    output_iec61850: GenerateSensorType(deviceTips.iec61850.value, sensorTips.output.value), 
    string_iec61850: GenerateSensorType(deviceTips.iec61850.value, sensorTips.string.value), 
    bitstring_iec61850: GenerateSensorType(deviceTips.iec61850.value, sensorTips.bitstring.value), 
    ieee_754_iec61850: GenerateSensorType(deviceTips.iec61850.value, sensorTips.ieee_754.value), 
    float_ping: GenerateSensorType(deviceTips.ping.value, sensorTips.float.value), 
    binary_ping: GenerateSensorType(deviceTips.ping.value, sensorTips.binary.value), 
    output_ping: GenerateSensorType(deviceTips.ping.value, sensorTips.output.value), 
    string_ping: GenerateSensorType(deviceTips.ping.value, sensorTips.string.value), 
    bitstring_ping: GenerateSensorType(deviceTips.ping.value, sensorTips.bitstring.value),
    ieee_754_ping: GenerateSensorType(deviceTips.ping.value, sensorTips.ieee_754.value), 
    float_telnet: GenerateSensorType(deviceTips.telnet.value, sensorTips.float.value), 
    binary_telnet: GenerateSensorType(deviceTips.telnet.value, sensorTips.binary.value), 
    output_telnet: GenerateSensorType(deviceTips.telnet.value, sensorTips.output.value), 
    string_telnet: GenerateSensorType(deviceTips.telnet.value, sensorTips.string.value), 
    bitstring_telnet: GenerateSensorType(deviceTips.telnet.value, sensorTips.bitstring.value),
    ieee_754_telnet: GenerateSensorType(deviceTips.telnet.value, sensorTips.ieee_754.value), 
    float_virtual: GenerateSensorType(deviceTips.virtual.value, sensorTips.float.value),
    binary_virtual: GenerateSensorType(deviceTips.virtual.value, sensorTips.binary.value),
    output_virtual: GenerateSensorType(deviceTips.virtual.value, sensorTips.output.value),
    string_virtual: GenerateSensorType(deviceTips.virtual.value, sensorTips.string.value),
    bitstring_virtual: GenerateSensorType(deviceTips.virtual.value, sensorTips.bitstring.value),
    ieee_754_virtual: GenerateSensorType(deviceTips.virtual.value, sensorTips.ieee_754.value),
    float_infrared: GenerateSensorType(deviceTips.infrared.value, sensorTips.float.value),
    binary_infrared: GenerateSensorType(deviceTips.infrared.value, sensorTips.binary.value),
    output_infrared: GenerateSensorType(deviceTips.infrared.value, sensorTips.output.value),
    string_infrared: GenerateSensorType(deviceTips.infrared.value, sensorTips.string.value),
    bitstring_infrared: GenerateSensorType(deviceTips.infrared.value, sensorTips.bitstring.value),
    ieee_754_infrared: GenerateSensorType(deviceTips.infrared.value, sensorTips.ieee_754.value),
    float_pcomm: GenerateSensorType(deviceTips.pcomm.value, sensorTips.float.value),
    binary_pcomm: GenerateSensorType(deviceTips.pcomm.value, sensorTips.binary.value),
    output_pcomm: GenerateSensorType(deviceTips.pcomm.value, sensorTips.output.value),
    string_pcomm: GenerateSensorType(deviceTips.pcomm.value, sensorTips.string.value),
    bitstring_pcomm: GenerateSensorType(deviceTips.pcomm.value, sensorTips.bitstring.value),
    ieee_754_pcomm: GenerateSensorType(deviceTips.pcomm.value, sensorTips.ieee_754.value)
};

readWrite   =  ["readonly", "writeonly", "read_write"];
Resolution  =  ["two_digit" ,"one_digit", "zero_digit"];
FilterType  =  ["resolution","changerate"];
alarmVals   =  ["binary_off", "binary_on"];
poweronVals =  ["no", "yes", "lastvalue"];
PeriodType  =  ["minute","hour","day","week","month","year"];

voltageOptions = {
	ia_conf_0_5v: 85,//                               0x55
	ia_conf_0_10v: 101, //                                  0x65
	ia_conf_0_6v: 149,//                                   0x95
	ia_conf_0_12v: 165//                                   0xA5
};
currentOptions = {
	ia_conf_4_20ma: 5, //                                  0x05
	ia_conf_0_20ma: 21, //                                   0x15
	ia_conf_0_24ma: 37, //                                  0x25
	ia_conf_0_20_4ma: 229, //                             0xE5
	ia_conf_0_24_5ma: 245, //                             0xF5
	ia_conf_3_92_20_4ma: 213 //                              0xD5	
};

function isVoltageInterval(interval)
{
	var val = parseInt(interval);
	for (key in voltageOptions)
	{
		if (voltageOptions[key] == val)
			return true;
	}
	return false;
}

function isCurrentInterval(interval)
{
	var val = parseInt(interval);
	for (key in vcurrentOptions)
	{
		if (vcurrentOptions[key] == val)
			return true;
	}
	return false;
}


var _base = 
[
    { name: "objectid", access: access.r, type: "s", html:"text"},
    { name: "ionum", access: access.r, type: "i", html:"text"}, 
    { name: "type", access: access.w, type: "i", html:"select", func:"createSensorTypeSelect();"},  
    { name: "name",  access: access.rw, type: "s", html:"text", disp:"input", dispref:"input"},
    { name: "read_write", access: access.w, type: "i", html:"select", func:"createReadWriteSelect();",disp:"select", dispref:"select"},
    { name: "enable", access: access.rw, type: "i", html:"checkbox", default:"1", disp:"select", dispref:"select"},
    //{ name: "read_enable", access: access.rw, type: "i", html:"checkbox", default:"1", disp:"select", dispref:"select"}, 
    { name: "record", access: access.rw, type: "i", html:"checkbox", default:"0", disp:"select", dispref:"select"}, 
    { name: "alarm_active", access: access.rw, type: "i", html:"checkbox", default:"1", disp:"select", dispref:"select"}, 
    { name: "record_period",  access: access.rw, type: "i", html:"text", default:"900", disp:"input", min:"10", max: "86400",  dispref:"input"}, 
    { name: "read_period",  access: access.rw, type: "i", html:"text", default:"300", disp:"input", min:"10", max: "86400",  dispref:"input"}
];

var _basestring = 
[
    { name: "objectid", access: access.r, type: "s", html:"text"},
    { name: "ionum", access: access.r, type: "i", html:"text"}, 
    { name: "type", access: access.w, type: "i", html:"select", func:"createSensorTypeSelect();"},  
    { name: "name",  access: access.rw, type: "s", html:"text", disp:"input", dispref:"input"},
    { name: "read_write", access: access.w, type: "i", html:"select", func:"createReadWriteSelect();",disp:"select", dispref:"select"},
    { name: "enable", access: access.rw, type: "i", html:"checkbox", default:"1", disp:"select", dispref:"select"},
    //{ name: "read_enable", access: access.rw, type: "i", html:"checkbox", default:"1", disp:"select", dispref:"select"}, 
    { name: "record", access: access.rw, type: "i", html:"checkbox", default:"0", disp:"select", dispref:"select"}, 
    { name: "record_period",  access: access.rw, type: "i", html:"text", default:"900", disp:"input", min:"10", max: "86400",  dispref:"input"}, 
    { name: "read_period",  access: access.rw, type: "i", html:"text", default:"300", disp:"input", min:"10", max: "86400",  dispref:"input"}
];


var _float = 
[
    { name: "unit",  access: access.w, type: "s", html:"text", default:" ", dispref:"input"},
    { name: "multiplier", access: access.rw, type: "f", html:"text", default:"1", disp:"input", dispref:"input"},
    { name: "offset", access: access.rw, type: "f", html:"text", default:"1", disp:"input", dispref:"input"},
    { name: "alarm_low",  access: access.rw, type: "f", html:"text", disp:"input", dispref:"input"},
    { name: "warning_low",  access: access.rw, type: "f", html:"text", disp:"input", dispref:"input"},
    { name: "warning_high",  access: access.rw, type: "f", html:"text", disp:"input", dispref:"input"},
    { name: "alarm_high",  access: access.rw, type: "f", html:"text", disp:"input", dispref:"input"},
    { name: "time_limit",  access: access.rw, type: "i", html:"text", default:"20", disp:"input", min:"0", dispref:"input"},
    { name: "hys_time_limit",  access: access.rw, type: "i", html:"text", default:"20", disp:"input", min:"0", dispref:"input"},
    { name: "filter_type", access: access.rw, type: "i", html:"select", func:"createFilterSelect();", disp:"select", dispref:"select"}, 
    { name: "resolution", access: access.rw, type: "i", html:"select", func:"createResolutionSelect();", disp:"select", dispref:"select"}, 
    { name: "change_rate", access: access.rw, type: "f", default:"0", html:"text", disp:"input", dispref:"input"}, 

];

var _binary = 
[
    { name: "time_limit",  access: access.rw, type: "i", html:"text", default:"20", disp:"input", min:"0", dispref:"input"},
    { name: "hys_time_limit",  access: access.rw, type: "i", html:"text", default:"20", disp:"input", min:"0", dispref:"input"},
    { name: "alarm_value", access: access.rw, html:"select", func:"createAlarmValSelect();", disp:"select", dispref:"select"},
    { name: "zero_name",  access: access.rw, type: "s", html:"text", disp:"input", dispref:"input"},
    { name: "one_name",  access: access.rw, type: "s", html:"text", disp:"input", dispref:"input"}
];

var _output = 
[
    { name: "poweron", access: access.rw, type: "i", html:"text", disp:"input", min:"0", dispref:"input"},
    { name: "apply",  access: access.rw, type: "i", html:"select" , func:"createPoweronAplySelect();", disp:"select", dispref:"select"}
];

var _bitstring = 
[
    { name: "length", access: access.rw, type: "i", html:"text", disp:"input", min:"2", dispref:"input"},
    { name: "mask", access: access.rw, type: "i", html:"text", disp:"input", min:"1", dispref:"input"}
];

var modbus = 
[
    { name: "regaddr", access: access.rw, type: "h", html:"text", disp:"spec", dispref:"spec"},
    { name: "regcnt", access: access.rw, type: "i", html:"text", disp:"spec", dispref:"spec"},
    { name: "read_function_code",  access: access.rw, type: "h", html:"text", disp:"spec", default:"", dispref:"spec", min: "0"},
    { name: "write_function_code", access: access.rw, type: "h", html:"text", disp:"spec",  default:"", dispref:"spec", min: "0"},
    { name: "issigned", access: access.rw, type: "i", html:"checkbox", default:"0" , disp:"spec", dispref:"spec"}
];

var meter = 
[
    { name: "obis_code", access: access.rw, type: "meterobis", html:"text", disp:"spec", dispref:"spec"}
];

var snmp = 
[
    { name: "oid", access: access.rw, type: "snmpoid", html:"text", disp:"spec", dispref:"spec"}
];

var iec61850 = 
[
    { name: "itemid", access: access.rw, type: "regexpattern", html:"text", disp:"spec", dispref:"spec"}
];

var ping = 
[
    { name: "ip", access: access.rw, type: "s", html:"text", disp:"spec", dispref:"spec"}
];

var telnet = 
[
    { name: "ip", access: access.rw, type: "ip", html:"text", disp:"spec", dispref:"spec"},
    { name: "port", access: access.rw, type: "i", html:"text", disp:"spec", max:"65535", min:"1", default:"23", dispref:"spec"}
];

var regex = 
[
	{ name: "pattern", access: access.rw, type: "regexpattern", html:"text", default:".*", disp:"spec", dispref:"spec"},
	{ name: "replace", access: access.rw, type: "regexpattern", html:"text", default:"$0", disp:"spec", dispref:"spec"}
];

var virtual = 
[
    { name: "varnum", access: access.r, type: "i", html: "text", disp: "spec", dispref: "spec"},
    { name: "variables", access: access.r, type: "s", html: "text", disp: "spec", dispref: "spec"},
    { name: "formula", access: access.r, type: "s", html: "text", disp: "spec", dispref: "spec"},
    { name: "periodtype", access: access.rw, type: "i", html:"select", func: "createPeriodTypeSelect();", disp: "spec", dispref: "select"},
    { name: "period", access: access.rw, type: "i", html: "text", disp: "spec", dispref: "spec"}
];

var infrared = 
[
    { name: "repeat_count", access: access.rw, type: "s", html: "text", disp: "spec", dispref: "spec"}
];

var pcomm = 
[
    { name: "sys_type", access: access.rw, type: "s", html: "text", disp: "spec", dispref: "spec"},
    { name: "sys_index", access: access.rw, type: "s", html: "text", disp: "spec", dispref: "spec"},
    { name: "subsys", access: access.rw, type: "s", html: "text", disp: "spec", dispref: "spec"},
    { name: "subsys_index", access: access.rw, type: "s", html: "text", disp: "spec", dispref: "spec"},
    { name: "data_obj", access: access.rw, type: "s", html: "text", disp: "spec", dispref: "spec"},
    { name: "data_type", access: access.rw, type: "s", html: "text", disp: "spec", dispref: "spec"},
    { name: "parser_type", access: access.r, type: "s", html: "text", disp: "spec", dispref: "spec"}
];

var float =  _base.concat(_float);
var binary =  _base.concat(_binary);
var output =  _base.concat(_output);
var string = _basestring;
var bitstring = _base.concat(_bitstring);
var ieee_754 = _base.concat(_float);

var sensorParams =
{
	float: float,
	binary: binary,
	output: output,
	string: string,
	bitstring: bitstring,
	ieee_754: ieee_754,
    float_modbus: float.concat(regex.concat(modbus)), 
    binary_modbus: binary.concat(regex.concat(modbus)), 
    output_modbus: output.concat(regex.concat(modbus)), 
    string_modbus: string.concat(regex.concat(modbus)), 
    bitstring_modbus: bitstring.concat(regex.concat(modbus)), 
   	ieee_754_modbus: ieee_754.concat(regex.concat(modbus)), 
    float_meter: float.concat(regex.concat(meter)), 
    binary_meter: binary.concat(regex.concat(meter)), 
    output_meter: output.concat(regex.concat(meter)), 
    string_meter: string.concat(regex.concat(meter)), 
    bitstring_meter: bitstring.concat(regex.concat(meter)), 
    ieee_754_meter: ieee_754.concat(regex.concat(meter)), 
    float_transparent: float.concat(regex),
    binary_transparent: binary.concat(regex),
    output_transparent: output.concat(regex),
    string_transparent: string.concat(regex),
    bitstring_transparent: bitstring.concat(regex),
    ieee_754_transparent: ieee_754.concat(regex),
    float_snmp: float.concat(regex.concat(snmp)),
    binary_snmp: binary.concat(regex.concat(snmp)),
    output_snmp: output.concat(regex.concat(snmp)),
    string_snmp: string.concat(regex.concat(snmp)),
    bitstring_snmp: bitstring.concat(regex.concat(snmp)),
    ieee_754_snmp: ieee_754.concat(regex.concat(snmp)),
    float_iec61850: float.concat(regex.concat(iec61850)),
    binary_iec61850: binary.concat(regex.concat(iec61850)),
    output_iec61850: output.concat(regex.concat(iec61850)),
    string_iec61850: string.concat(regex.concat(iec61850)),
    bitstring_iec61850: bitstring.concat(regex.concat(iec61850)),
    ieee_754_iec61850: ieee_754.concat(regex.concat(iec61850)),
    float_ping: float.concat(ping),
    binary_ping: binary.concat(ping),
    output_ping: output.concat(ping),
    string_ping: string.concat(ping), 
    bitstring_ping: bitstring.concat(ping),
    ieee_754_ping: ieee_754.concat(ping),
    float_telnet: float.concat(telnet),
    binary_telnet: binary.concat(telnet),
    output_telnet: output.concat(telnet),
    string_telnet: string.concat(telnet),
    bitstring_telnet: bitstring.concat(telnet),
    ieee_754_telnet: ieee_754.concat(telnet),
    float_virtual: float.concat(virtual),
    binary_virtual: binary.concat(virtual),
    output_virtual: output.concat(virtual),
    string_virtual: string.concat(virtual),
    bitstring_virtual: bitstring.concat(virtual),
    ieee_754_virtual: ieee_754.concat(virtual),
    float_infrared: float.concat(infrared), 
    binary_infrared: binary.concat(infrared), 
    output_infrared: output.concat(infrared), 
    string_infrared: string.concat(infrared), 
    bitstring_infrared: bitstring.concat(infrared), 
    ieee_754_infrared: ieee_754.concat(infrared),
    float_pcomm: float.concat(pcomm),
    binary_pcomm: binary.concat(pcomm),
    output_pcomm: output.concat(pcomm),
    string_pcomm: string.concat(pcomm),
    bitstring_pcomm: bitstring.concat(pcomm),
    ieee_754_pcomm: ieee_754.concat(pcomm),
}


function isIEEE754 (type) {
	var sType = parseInt(type);
	return (sType == sensorTypes.ieee_754 || sType == sensorTypes.ieee_754_modbus || sType == sensorTypes.ieee_754_meter || 
	       sType == sensorTypes.ieee_754_transparent || sType == sensorTypes.ieee_754_snmp || sType == sensorTypes.ieee_754_iec61850 || 
	       sType == sensorTypes.ieee_754_ping || sType == sensorTypes.ieee_754_telnet || sType == sensorTypes.ieee_754_virtual || 
           sType == sensorTypes.ieee_754_infrared || sType == sensorTypes.ieee_754_pcomm );
} 

function isFloat (type) {
	var sType = parseInt(type);
	return (sType == sensorTypes.float || sType == sensorTypes.float_modbus || sType == sensorTypes.float_meter || 
	       sType == sensorTypes.float_transparent || sType == sensorTypes.float_snmp || sType == sensorTypes.float_iec61850 || 
	       sType == sensorTypes.float_ping || sType == sensorTypes.float_telnet || sType == sensorTypes.float_virtual || 
           sType == sensorTypes.float_infrared || sType == sensorTypes.float_pcomm) || isIEEE754(type);
} 


function isBinary (type) {
	var sType = parseInt(type);
	return (sType == sensorTypes.binary || sType == sensorTypes.binary_modbus || sType == sensorTypes.binary_meter || 
	       sType == sensorTypes.binary_transparent || sType == sensorTypes.binary_snmp || sType == sensorTypes.binary_iec61850 || 
	       sType == sensorTypes.binary_ping || sType == sensorTypes.binary_telnet || sType == sensorTypes.binary_virtual || 
           sType == sensorTypes.binary_infrared || sType == sensorTypes.binary_pcomm );
} 


function isOutput (type) {
	var sType = parseInt(type);
	return (sType == sensorTypes.output || sType == sensorTypes.output_modbus || sType == sensorTypes.output_meter || 
	       sType == sensorTypes.output_transparent || sType == sensorTypes.output_snmp || sType == sensorTypes.output_iec61850 || 
	       sType == sensorTypes.output_ping || sType == sensorTypes.output_telnet || sType == sensorTypes.output_virtual || 
           sType == sensorTypes.output_infrared || sType == sensorTypes.output_pcomm );
} 


function isOutputAndBinary (type) {
    var sType = parseInt(type);
    return (sType == sensorTypes.output);
} 

function isString (type) {
	var sType = parseInt(type);
	return (sType == sensorTypes.string || sType == sensorTypes.string_modbus || sType == sensorTypes.string_meter || 
	       sType == sensorTypes.string_transparent || sType == sensorTypes.string_snmp || sType == sensorTypes.string_iec61850 || 
	       sType == sensorTypes.string_ping || sType == sensorTypes.string_telnet || sType == sensorTypes.string_virtual || 
           sType == sensorTypes.string_infrared || sType == sensorTypes.string_pcomm );
} 

function isBitString (type) {
	var sType = parseInt(type);
	return (sType == sensorTypes.bitstring || sType == sensorTypes.bitstring_modbus || sType == sensorTypes.bitstring_meter || 
	       sType == sensorTypes.bitstring_transparent || sType == sensorTypes.bitstring_snmp || sType == sensorTypes.bitstring_iec61850 || 
	       sType == sensorTypes.bitstring_ping || sType == sensorTypes.bitstring_telnet || sType == sensorTypes.bitstring_virtual || 
           sType == sensorTypes.bitstring_infrared || sType == sensorTypes.bitstring_pcomm);
} 


sensorTypesToStr = function(type)
{
	for (k in sensorTypes)
	{
		if (sensorTypes[k] == parseInt(type))
		{
			return k;
			/*if (!isIEEE754(type))
				return k;
			else
			{
				var r = k.split("_");
				r = "float_" + r[2];
				return r;
			}*/	
		}
			
	}
};

sensorTypesToStr2 = function(type)
{
	for (k in sensorTypes)
	{
		if (sensorTypes[k] == parseInt(type))
		{
			return k.replace("ieee_754", "float");
		}
	}
};

strToSensorType = function(type)
{
	for (k in sensorTypes)
	{
		if (k == type)
			return sensorTypes[k];
	}
};

DataTypeFromSensType = function(type)
{
	//return ((parseInt(type)%5) == 0) ? 5:(parseInt(type)%5);
	var type = (parseInt(type) << 16) >> 16;
/*	if (type == 6)
		type = 5;*/
	return type;
};

DeviceTypeFromSensType = function(type)
{
	//return ((parseInt(type)%5) == 0) ? 5:(parseInt(type)%5);
	return (parseInt(type) >> 16);
};
//////////////////////// END OF SENSORS ////////////////////////


/////////////////////////////////////////////////////////////

MDLMNGR = {
	devices : {name: "devices", params: deviceParams},
	connectors : {name: "connectors", params: connectorParams},
	ports : {name: "ports", params: portParams},
	sensors: {name: "sensors", params: sensorParams},
}




///////////////////// ADAPTORS  /////////////////////////////////////////


var _snmp = [
        { name: "port", access: access.w, type: "i", html:"text", max:"65535", min:"1"},
        { name: "version", access: access.w, type: "i", html:"select", func:"createSnmpVersionSelect();"},
        { name: "community_name", access: access.rw, type: "s", html:"text"},
        { name: "security_name", access: access.rw, type: "s", html:"text", default:" "},
        { name: "password", access: access.rw, type: "s", html:"password", default:" "},
        { name: "ref_file", access: access.w, type: "s", html:"select",func:"getRefFiles();"}
    ];

var _iec60870 = [
    ];

var exportTypes = {};
var exportFormats = {};

exportTypesToStr = function(type)
{
    for (k in exportTypes)
    {
        if (exportTypes[k] == parseInt(type))
        {
            return k;
        }
    }
};

exportParams = 
{
    //snmp: _snmp,
    iec60870: _iec60870
};



//////////////////////// END OF ADAPTORS //////////////////////////////