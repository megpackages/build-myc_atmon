#!/usr/bin/python

import smtplib,string,sys,os
from time import gmtime,strftime,sleep
import syslog
import getopt
import socket
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders

SUCCESFULLY_SENDED 		 = 0
LOGIN_FAILED			 = 1
EMAIL_CANNOT_BE_SENDED	 = 2
MANDATORY_PARAMS_MISSING = 3
LAST_EXCEPTION			 = ""

SSL_ENABLED = False
SERVER      = ""
PORT		= ""
USERNAME	= ""
LOGINNAME	= ""
PASSWORD	= ""
MSG			= ""
SUBJ		= ""
TO			= ""
FILES       = ""
RECEIPT     = False
def unicodeChanger(c):
	return unicode(c).encode("utf-8")

def getParams(argv):
	global SSL_ENABLED, SERVER, PORT, USERNAME, LOGINNAME, PASSWORD, MSG, SUBJ, TO,FILES, RECEIPT
	global EMAIL_CANNOT_BE_SENDED, MANDATORY_PARAMS_MISSING,LAST_EXCEPTION
	try:
		opts, args = getopt.getopt(sys.argv[1:],"hs:p:at:m:i:u:l:c:f:r",[])
	except getopt.GetoptError:
		LAST_EXCEPTION = "Getopt Error"
		raise Exception(EMAIL_CANNOT_BE_SENDED)
	
	server, port, msg, to ,subj = False, False,False, False,False
	username, loginname, password = False, False, False
	
	for opt, arg in opts:
		if opt == '-h':
			print 'sendmail.py -s <server> -p <port> -t <tos> -m <message> -i <subject> -f <files> -a(optional ssl enabled) -u (username if -a exists) -l (loginname if -a exists) -c (password if -a exists) -r (optional receipt enable) '
			sys.exit(0)
		elif opt == '-p':
			PORT = arg
			port = True
		elif opt == '-s':
			SERVER = arg
			server = True
		elif opt == '-a':
			SSL_ENABLED = True
		elif opt == '-t':
			TO = arg
			to = True
		elif opt == '-m':
			MSG = arg
			msg = True
		elif opt == '-i':
			SUBJ = arg
			subj = True
		elif opt == '-u':
			USERNAME = arg
			username = True
		elif opt == '-f':
			FILES = arg.split(',')
			username = True
		elif opt == '-r':
			RECEIPT = True
		
	if (not server) or (not port) or (not msg) or (not to) or (not subj):
		error = "mandatory parameter(s) are missing: " + "" if server else "-s mail_server " + "" if port else "-p mail_server_port " + "" if msg else "-m message" + "" if subj else "-i subject" + "" if to else "-t to";
		syslog.syslog(syslog.LOG_ALERT, error)
		LAST_EXCEPTION = error
		raise Exception(MANDATORY_PARAMS_MISSING)
	
	if SSL_ENABLED == True:
		for opt, arg in opts:
			if opt == '-l':
				LOGINNAME = arg
				loginname = True
			elif opt == '-c':
				PASSWORD = arg
				password = True
		
		if not username:
			USERNAME = LOGINNAME
			
		if not loginname or not password:
			error = "mandatory parameter(s) are missing for SSL: " + "" if username else "-l loginname " + "" if password else "-c password";
			syslog.syslog(syslog.LOG_ALERT, error)
			LAST_EXCEPTION = error
			raise Exception(MANDATORY_PARAMS_MISSING)
	  	
def main(argv):
	global SSL_ENABLED, SERVER, PORT, USERNAME, LOGINNAME, PASSWORD, MSG, SUBJ, TO, RECEIPT
	global EMAIL_CANNOT_BE_SENDED, MANDATORY_PARAMS_MISSING,LAST_EXCEPTION
	syslog.openlog("EMAIL")
	try:
		getParams(sys.argv[1:])
		socket.setdefaulttimeout(10)
		smtp = None
		if SSL_ENABLED:
			smtp = AuthenticatedLogin()		
		else:
			smtp = UnAuthenticatedLogin()
		Send(smtp)
		sys.exit(SUCCESFULLY_SENDED)
	except Exception, err:
		print LAST_EXCEPTION
		sys.exit(int(str(err)))
	
	
def AuthenticatedLogin():
	global SSL_ENABLED, SERVER, PORT, USERNAME, LOGINNAME, PASSWORD, MSG, SUBJ, TO
	global EMAIL_CANNOT_BE_SENDED, MANDATORY_PARAMS_MISSING,LAST_EXCEPTION
	smtpserver = None
	syslog.syslog(syslog.LOG_DEBUG, "authenticated login will be done")
	try:  #bu try catch, login olup ssl kabul etmeyen durumlari handle eder
		smtpserver = smtplib.SMTP(SERVER,PORT)
		smtpserver.ehlo()
		smtpserver.starttls()
		smtpserver.ehlo
		smtpserver.login(LOGINNAME,PASSWORD)
		syslog.syslog(syslog.LOG_DEBUG, "SMTP Sunucusuna Basari Ile SSL kullanarak Login Oldu. Kullanici Adi: " + USERNAME) ;
	except Exception, err:
		syslog.syslog(syslog.LOG_ALERT, "SSL ile login olamadi: " + str(err))
		try:			
			smtpserver.login(LOGINNAME,PASSWORD) #ssl ile baglanmadan login ol
			syslog.syslog(syslog.LOG_DEBUG, "SMTP Sunucusuna Basari Ile SSL kullanmadan Login Oldu. Kullanici Adi:  " + LOGINNAME) ;
		except Exception, err:
			syslog.syslog(syslog.LOG_ALERT, "Login to mail server " + SERVER + " with authentication failed: " + str(err))
			LAST_EXCEPTION = "Login to mail server " + SERVER + " with authentication failed: " + str(err)
			raise Exception(LOGIN_FAILED)
	return smtpserver

def UnAuthenticatedLogin():
	global SSL_ENABLED, SERVER, PORT, USERNAME, LOGINNAME, PASSWORD, MSG, SUBJ, TO
	global EMAIL_CANNOT_BE_SENDED, MANDATORY_PARAMS_MISSING,LAST_EXCEPTION
	smtpserver = None
	syslog.syslog(syslog.LOG_DEBUG, "un-authenticated login will be done")
	try:
		smtpserver = smtplib.SMTP(SERVER,PORT)
	except Exception,err:
		syslog.syslog(syslog.LOG_ALERT, "Login to mail server " + SERVER + " without authentication failed: " + str(err))
		LAST_EXCEPTION = "Login to mail server " + SERVER + " without authentication failed: " + str(err)
		raise Exception(LOGIN_FAILED)
	return smtpserver
	
def Send(smtpserver):
	global SSL_ENABLED, SERVER, PORT, USERNAME, LOGINNAME, PASSWORD, MSG, SUBJ, TO, FILES, RECEIPT
	global EMAIL_CANNOT_BE_SENDED, MANDATORY_PARAMS_MISSING,LAST_EXCEPTION
	syslog.syslog(syslog.LOG_DEBUG, "subject: " + str(SUBJ) + " msg:" + str(MSG))
	try:
		############## PREPARE MESSAGE ############
		msg = MIMEMultipart()
		msg['From'] = USERNAME
		msg['To'] = TO
		msg['Date'] = formatdate(localtime=True)
		msg['Subject'] = SUBJ
	
		if (RECEIPT):
			msg['Disposition-Notification-To'] = USERNAME
		
		msg.attach( MIMEText(MSG, "plain", "utf-8"))
		
		for f in FILES:
			if f == "":
				continue
			try:
				fp = open(f, 'rb')
				part = MIMEText(fp.read(), "plain", "utf-8")
				fp.close
				part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
				msg.attach(part)
			except Exception,err:
				print str(err)
		###########################################
		
		syslog.syslog(syslog.LOG_DEBUG, "Before send mail");
		smtpserver.sendmail(USERNAME, TO, msg.as_string())
		smtpserver.close()
		syslog.syslog(syslog.LOG_DEBUG, "Email Basari Ile Gonderildi. Gonderilenler: " + str(TO));		
	except Exception, err:
		syslog.syslog(syslog.LOG_DEBUG, "Email Gonderilemedi:" +  str(err));
		LAST_EXCEPTION = "Email Gonderilemedi:" +  str(err)
		raise Exception(EMAIL_CANNOT_BE_SENDED)

if __name__ == "__main__":
   main(sys.argv[1:])
