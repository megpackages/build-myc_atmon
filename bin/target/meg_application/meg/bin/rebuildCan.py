#!/usr/bin/python 

import os
import subprocess
from time import sleep

canList = [
        'ip link set can0 down', 
        'echo  50000 > /sys/devices/platform/FlexCAN.0/bitrate', 
        'echo  19 > /sys/devices/platform/FlexCAN.0/br_presdiv', 
        'echo  8 > /sys/devices/platform/FlexCAN.0/br_propseg', 
        'echo  8 > /sys/devices/platform/FlexCAN.0/br_pseg1', 
        'echo  7 > /sys/devices/platform/FlexCAN.0/br_pseg2', 
        'echo  2 > /sys/devices/platform/FlexCAN.0/br_rjw', 
        'echo  1 > /sys/devices/platform/FlexCAN.0/smp',       
        'ip link set can0 up'
        ]

for i in canList:
    os.system(i)
