#!/bin/sh +x

Hwaddr="n/a"
IpAddr="n/a"
SubnetMask="n/a"
GateWay="n/a"
dhcpEnabled="0"
dnsIp="n/a"

Hwaddr=$(ifconfig eth0 | grep -ioE 'Hwaddr (.[^$ ]*)' | grep -ioE '[^hwaddr ].*')
IpAddr=$(ifconfig eth0 | grep -ioE 'inet addr:(.[^$ ]*)' | grep -ioE '[^inet addr:].*')
SubnetMask=$(ifconfig eth0 | grep -ioE 'mask:(.[^$ ]*)' | grep -ioE '[^mask:].*')
GateWay=$(route -n | grep -oiE ".*UG.*$" | grep -oiE "([ \t])*[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}" |  grep -oiE "([^ \t])[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}") 
dhcpEnabled=$(cat /etc/network/interfaces | grep -o "iface eth0 inet dhcp")
dnsIp=$(cat /etc/resolv.conf | cut -f2 -d' ')
if [ "$dhcpEnabled" = "iface eth0 inet dhcp" ];then
	dhcpEnabled="1"
else
    dhcpEnabled="0"
fi

Result="$Hwaddr#$IpAddr#$SubnetMask#$GateWay#$dhcpEnabled#$dnsIp#"
echo $Result
